+++
title = "Edita esta web"
description = "Pasos a seguir para editar esta web"
date = 2022-05-28T16:07:44+02:00
weight = 20
archetype = "section"
pre = "<b><i class='fas fa-edit'></i> </b>"
tags = [
    "Web estática",
    "Hugo"
]
toc = true
+++

# ¿Cómo editar esta web?

En esta sección se explica cómo contribuir o editar esta web estática generada con Hugo y desplegada con GitLab Pages. Existen diferentes maneras de editar la web, por lo que se exponen las diferentes formas en función de los conocimientos técnicos.

## Tabla de Contenidos

{{% children depth="4" /%}}