+++
title = "Decisiones"
description = "Decisiones del HackLab"
date = 2022-09-04T16:07:44+02:00
weight = 10
chapter = false
pre = "<b><i class='fas fa-info-circle'></i> </b>"
tags = [
]
toc = true
+++

## Compendio de decisiones del hacklab

## Reparto de las tareas del centro social

Como colectivo de la Ferro nos comprometemos a:

* Asistir a las asambleas de la ferro (aproximadamente cada 10 días). Para ello
    hemos creado un [documento para
    coordinarnos](https://cryptpad.disroot.org/sheet/#/2/sheet/edit/I0UE-oF9nrl4Na4L7xRd32FE/)
    y se propuso que cada semana se acordara sobre quien puede ir.

* Estar en el grupo de Whassap de la Ferro.
    * Se acuerda en rotarse cada mes quién está en el grupo
    * Quedamos en apuntar las decisiones tomadas en el pad

* Hacer cafetas y limpieza del centro cuando nos toque (aproximadamente cada 2 o 3 meses).
    Se nos ocurre que podemos coordinarlo con uno de los siguientes talleres:

    * Jornadas de privacidad
    * Taller de lockpicking y bricolage
    * Taller de intrusión física

    Normalmente se suele hacer la limpieza después de la cafeta.

* Se nos pidió que fuésemos comentando qué íbamos a hacer los miércoles por el
    grupo de difusión de la Ferro, pero aún no lo hemos hecho.

## Las asambleas del hacklab

- Se empieza cuando seamos 8 personas o un 50% de las confirmadas.
- Escribir el orden del día con antelación y que sea lo más corto posible
- Duración máxima dos tiempos de 45 minutos con un descanso entre medias.
