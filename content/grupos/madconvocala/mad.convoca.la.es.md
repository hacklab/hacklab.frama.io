+++
title = "🔴 ¿Qué es mad.convoca.la? 🔥"
description = "¿Quieres saber más o conocernos?"
date = 2023-05-05T16:07:44+02:00
weight = 10
chapter = false
tags = [
]
toc = true
+++

Es un colectivo (de colectivos) que divulga prácticas de **soberanía comunicativa**. Actualmente estamos impulsando una **agenda política y cultural autónoma**, que quiere contribuir a **difundir y coordinar acciones** públicas de colectivos sociales y políticos en **Madrid**.

### 🌐 Ejes de mad.convoca.la 📣
- La okupación digital de espacios de **comunicación y autoorganización**.
- Apoyo a la **difusión comunitaria** de los movimientos sociales.
- Por la comunicación entre colectivos, **por entrelazarnos más y pisarnos menos**.
- Para **facilitar la planificación** personal de las activistas.

### 💻 Tecnología 🛠
- **Autogestionada**: que devuelve la gestión de contenidos a la comunidad y colectivos los cuales generan estas propuestas, tanto culturales y sociales.
- **Independiente (que no excluyente)**: una *respuesta alternativa* a las plataformas como Facebook, Instagram, Telegram o Twitter. Que hacen vigilancia masiva, censura política, manipulación social (publicidad), y control de contenido que es poco transparente y se ha comprobado que favorece a los poderes económicos.
- **Respetuosa con la privacidad**: minimización de datos recogidos, identidades colectivas, publicaciones anónimas.
- **Autónoma**: utilizamos una herramienta de código libre y colaborativa ([Gancio](https://gancio.org/)), alojada en servidores radicales ([Sin Dominio](https://sindominio.net/)).

### 🤔 ¿Por dónde empezar? ▶️
- Vente a la [**asamblea autónoma online o itinerante**](<https://mad.convoca.la/tag/Mad Convócala>) por distintos espacios.
- Visita la **web [https://mad.convoca.la](https://mad.convoca.la)**.
- En el **fediverso**, sigue la cuenta `@madrid@mad.convoca.la`.
- Sigue el **canal de telegram** [t.me/madconvocala](https://t.me/madconvocala)
- Importa el **calendario remoto** ICS/ICAL: [https://mad.convoca.la/feed/ics?show_recurrent=true](https://mad.convoca.la/feed/ics?show_recurrent=true)
- Añade este enlace a su **lector de feeds RSS**: [https://mad.convoca.la/feed/rss?show_recurrent=true](https://mad.convoca.la/feed/rss?show_recurrent=true)

### 📆 ¿Cuándo? ⏰
Todos los días del año 🔥

Para saber más sobre el proyecto, aquí tienes toda la información en esta web y en [https://mad.convoca.la/about](https://mad.convoca.la/about).
