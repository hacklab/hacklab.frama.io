+++
title = "Digital commons or how to squat digital territories"
description = "Reflections on communicative sovereignty and how to occupy digital territories"
date = 2023-05-05T16:07:44+02:00
weight = 10
chapter = false
tags = [
]
toc = true
+++

A **Self-managed Social Center**, also known as autonomous social centers, is organized around a physical space and a group of people. It is best understood as a *verb*, *from action*, in a *context* and with a *shared vision*.

In many occasions the practice of autonomous social centers consists of living (in) and defending (a) space where diverse links are cultivated and entangled resulting in the creation of knots in the history of our neighborhoods, towns... territories.

In the current context, where large technological corporations are taking over the channels of diffusion and acting as mediating agents of the contents they distribute, we find that the practice outside the physical space of the CSA is totally subjugated to the digital territories we inhabit. How to deploy strategies akin to our values that free us from these systematic oppressions? 

We come to talk about these strategies. We come to propose that autonomous social centers **squat a digital space** to help liberate the people who give them life from many (already systemic) oppressions.

## The muzzle inside the computer, in the code. Communicative sovereignty?

Let us imagine for a moment that we could only write on paper, that we wanted to write a personal or collective diary, something that is important to us, to our memory. Let's imagine now that we only have at our disposal a notebook in which to write, even worse, let's imagine that this notebook is not ours nor will it ever be in our possession. Let's imagine that "an authority" will be the one who determines when we can write in that notebook because he will be at our side all the time. Not only that, he will also be watching what we write so he will decide what we can and cannot write.

> **"Hi, my name is Instagram and I don't want nipples on my notebook "**.

At this point we have reached, digital territories are totally dominated by big tech corporations and that, unfortunately, affects the way we imagine ourselves and the ways in which we interact (social networks, schools, buying and selling...).

We want to find a balance between the strengths we have at our disposal and the multiple struggles we face. We need cross-cutting solutions that change our practice in such a way that we emerge stronger.

The central question in this section is: **are we sovereign in our communication?** do we have the right strategies and tactics to tell our story without being accountable to third parties?

At this point, we do not want to limit ourselves to thinking digitally. The digital environment is just another medium through which to communicate and we do not intend to downplay the importance of the rest, quite the contrary. What we seek with these proposals is to ensure that the digital medium does not take over and end up shaping our story.

And for this we want to **problematize** briefly **communication in digital environments**.
With the above metaphor, some characteristics of the communication model in which we are immersed come to light:

- The media do not belong to us and as a consequence we *depend on digital platforms* to reach our colleagues. It is interesting how we are forced to adapt ourselves even to the imposed format: image, text, video...    

- *Contents are stored on big companies' computers* and not on ours when we allow interactions (in digital territories) to happen on the aforementioned platforms. How many Facebook groups, Instagram profiles, Telegram groups contain the memory of our collectives? And if tomorrow these platforms decide to close our accounts, what will we do then? But it's not just an issue of the future, how do we do it now, who hasn't had problems with large project accounts, how do we manage credentials, what gets published and what doesn't, whether the voice is collective or an individuality. All of this constraint is rooted in the code and already seems like an assumed oppression.

## Imagining collective and self-managed escape routes

From [mad.convoca.la](https://mad.convoca.la) we believe in meeting, in dialogue, we want to see each other more, to know each other better and this is not an easy task in the world we live in. That is why we believe that a first step could be to appropriate an agenda. An agenda in which to place the most important dates, where the diversity of struggles and the **circumstances of each one** can be included. 

We think of **an agenda as a tool** to manage time and we are aware that in a capitalist world time is too scarce a resource. When we say agenda we do not think of an agenda as we imagine it now, a single notebook, managed by "an authority". We need each collective to be able to define its own limits, to have sovereignty over what it aims at and how it aims at it. We have the keys for this to change (we have people who know how to program, people who have participated or participate in the media, in movements for communicative sovereignty...), maybe we need to give it a name to get out of the current frame of reference. This new agenda will not be a notebook, *it will be a common or it will not be*.

We propose to **think and live it as a common** because we need the meeting of people from different collectives. We propose that an assembly, a charter of values and a space (in this case a digital one) should set the rules of "our" agenda. Therefore, concepts that are familiar to us appear and that do show us the difficulties that such an initiative can entail. The governance of the project is a central issue, together with the representativeness of the assembly and its accessibility. All this talking about an initiative that seeks to bring the movements together, so it must have the capacity to bring them together.

At the same time we talk about the need not to centralize communication, so this agenda is nothing more than a complementary tool that allows to add redundancy to the more local communication channels of each collective. It is a loudspeaker, but it should not force us to duplicate publication efforts or entrust our memory to a computer outside our collective (as happens in social networks).

## Aiming to mobilize

In order to carry out this strategic action that will allow us to gradually strengthen our communicative sovereignty, we propose to rethink our digital tactics beyond the screens.
    
mad.convoca.la is presented as a **itinerant and open assembly** (based on the Ferroviaria hacklab), with a vocation to listen to the communicative realities of our movements, to disseminate new practices of use of digital technologies and to accompany in the process of transformation. 

It is present in the digital territories as a public and federated agenda of activist and cultural events. It consists of a website that replicates the events of the pages of social collectives, allows you to publish anonymously if you do not have infrastructure, has a fediverse profile and a Telegram channel in addition to the possibility of subscribing by RSS, synchronize calendars by ICS/ICAL or embed your events on other pages through a web component. 

If you are interested in starting to use the tool, we invite you to come to one of our assemblies to meet us in person and provide you with any information you need. Or contact us via email at <a href="mailto:mad.convocala<arroba>sindominio.net">`mad.convocala<arroba>sindominio.net`</a>.