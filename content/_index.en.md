+++
author = "HackLab"
title = "HackLab UniPopular"
description = "HackLab Uni Popular"
weight = 1
draft = false
date = 2022-05-28T16:07:44+02:00
archetype="home"
+++

![Logo](../logos/logo_150x150.jpg?height=150&classes=shadow)

## ¿Who are we?

A group of people who meet each other to form an interdisciplinary space for cooperative learning and technological tinkering using free software.

## How to meet us?

You can reach us in Telegram, joining the group Interhacklabs:

{{% button href="https://t.me/+EmWc6-CNLPJlMTc0" style="secondary" icon="fab fa-telegram" iconposition="right" %}}Join us in Telegram!{{% /button %}}

Or coming to visit us to any of our currently active spaces:

- [0day](https://0day.lol) (e.s.l.a. Eko)
- Teknokasa (CSO La Rosa)
- La raíz (CSOA La Enre)

{{< hacklab-events >}}

## Table of Contents

{{% children description="true" depth="4" /%}}
