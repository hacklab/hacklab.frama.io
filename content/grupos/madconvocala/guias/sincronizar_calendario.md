+++
title = "¿Cómo importar el calendario remoto?"
description = "Guía de cómo sincronizar el calendario en mad.convoca.la"
date = 2024-01-14T18:07:44+02:00
weight = 16
chapter = false
tags = [
]
toc = true
+++

En esta guía táctica describiremos cómo **importar el calendario de [mad.convoca.la](mad.convoca.la)** con varias aplicaciones de calendario, ya sea en el móvil o en el ordenador. Aprenderemos a importar un evento de forma individual, varios eventos filtrados por etiquetas o ubicación y todos los eventos globalmente.

## ¿Cómo se importa un calendario remoto cualquiera?

Para comenzar, presentamos los conceptos de iCalendar, Webcal y  para entender en qué se basan los calendarios digitales. Es la parte teórica que nos ayudará a entender qué estamos haciendo y en qué se fundamenta. Continuaremos con la parte práctica y los pasos de cómo sincronizar calendarios.

Como resumen teórico y para hacernos un mapa mental de los componentes que intervienen en la importación de calendarios remotos, tenemos que saber lo siguiente.
- **iCalendar es un estándar de formato para el intercambio de información de calendarios**.
- **Webcal es un estándar para acceder a archivos con formato iCalendar a través de WebDAV**.
- **WebDAV es un protocolo basado en HTTP que proporciona funcionalidades para crear, cambiar y mover documentos o en un servidor remoto**.
- **CalDAV es un estándar que extiende WebDAV y utiliza iCalendar como formato para los datos para permitir a un cliente acceder a información de planificación en un servidor remoto**.
  
### ¿Qué es iCalendar?

**iCalendar** es un **estándar** para el *intercambio de información de calendarios*, también se conoce como "iCal". Permite a les usuaries *invitar a eventos* o asignar tareas a otres usuaries generalmente a través del correo electrónico. La persona destinataria del mensaje en formato iCalendar (de tener un programa que lo permita) puede *responder fácilmente aceptando la invitación, o proponiendo otra fecha y hora para la misma*. 

La **información** en formato iCalendar puede ser **compartida y editada** utilizando un servidor WebDAV. Un *servidor web sencillo* (usando tan solo el protocolo HTTP) puede ser usado para **distribuir la información de un evento** en particular, La información iCalendar es declarada como tipo de contenido MIME text/calendar. Se debe usar la **extensión "ics"** para nombrar un archivo que tenga (una cantidad cualquiera de) información de calendario y agenda consistente con este tipo de contenido MIME[^1].

[^1]: [Wikipedia, iCalendar](https://es.wikipedia.org/wiki/ICalendar)

### ¿Qué es Webcal?

**Webcal** fue ideado para su uso con la aplicación iCal de Apple y se ha convertido en un **estándar común de facto para acceder a archivos con formato iCalendar a través de WebDAV**. Específicamente es un esquema de *identificador de recursos uniforme (URI)* para *acceder a archivos iCalendar*. WebCal *permite crear y mantener un calendario de eventos interactivos* en un sitio web o aplicación[^2] a través de WebDAV.

El prefijo de protocolo Webcal se utiliza para activar un controlador de protocolo externo que se pasa la URL del archivo .ics en lugar de ser pasado el contenido descargado del archivo, de la misma manera que la alimentación se utiliza a veces para activar los lectores RSS externos. La idea es que con este prefijo de protocolo se suscriba al archivo de destino en lugar de importarlo a la aplicación de calendario, como ocurriría con una simple descarga [^2].

[^2]:[Wikipedia, Webcal](https://en.wikipedia.org/wiki/Webcal)

### ¿Qué es WebDAV y CalDAV?

**WebDAV** (Web Distributed Authoring and Versioning) es un protocolo basado en HTTP que proporciona funcionalidades para crear, cambiar y mover documentos o en un servidor remoto (típicamente un servidor web). Se utiliza sobre todo para permitir la edición de los documentos que sirve un servidor web, pero puede también aplicarse a sistemas de almacenamiento generales basados en web, que pueden ser accedidos desde cualquier lugar[^3].
[^3]:[Wikipedia, WebDAV](https://es.wikipedia.org/wiki/WebDAV)

**CalDAV** (Calendaring Extensions to WebDAV) es un estándar que permite a un cliente acceder a información de planificación en un servidor remoto. Extiende WebDAV y utiliza iCalendar como formato para los datos. Permite que varios clientes accedan a la misma información, facilitando la cooperación. Muchas aplicaciones, tanto clientes como servidores, son compatibles con este protocolo[^4].
[^4]:[Wikipedia, CalDAV](https://en.wikipedia.org/wiki/CalDAV)

## ¿Cómo importar un calendario remoto en el móvil?

A continuación veremos paso por paso cómo importar calendarios en un teléfono móvil. Primero seleccionaremos una entre las distintas aplicaciones de calendario. En nuestro caso práctico, para hacer esta selección tenemos el requisito de poder importar un calendario remoto. 
Para Android, entre las más conocidas y de software privativo destaca Google Calendar. Alternativamente, otras menos conocidas y de software libre son Etar o Fossify Calendar. 

Concretamente, guiaremos sobre cómo importar mad.convoca.la en Etar, la cual requiere F-Droid para ser instalada e ICSx5 para subscribirse a la actividad de Webcal.

### Paso 1 · Descarga F-Droid

{{< figure src="/images/madconvocala/guias_tacticas/f-droid.png" >}}

Desde tu móvil ve a [f-droid.org](https://f-droid.org/) y descarga la .apk o descarga directamente desde [este enlace](https://f-droid.org/F-Droid.apk). Dirígete a descargas para instalarla. Te pedirá que des permisos a tu navegador para instalar aplicaciones de esa fuente. Quizás te asustes en un principio y pienses que te estamos engañando y es un virus. Este aviso es debido a que Google no le gusta que seas una persona subversiva que sale de sus dominios y sus garras. 

 Acepta el permiso y cuando termines de instalar F-Droid es recomendable que le vuelvas a quitar el permiso al navegador. Al instalarla te pedirá que aceptes apps de terceros. Dí que si. Confía en esta. F-Droid es un catálogo de aplicaciones libres, como PlayStore pero mucho mejor. 
### Paso 2 · Busca e instala ICSx5

Abre y busca en F-Droid la app `ICSx5`.

### Paso 3 · Ir a Compartir en mad.convoca.la

{{< figure src="/images/madconvocala/guias_tacticas/gancio-calendar.png" >}}

Vamos a compartir https://mad.convoca.la/export y en menú de abajo copiamos la dirección de ICS/CAL

### Paso 4 · Abrimos ICSx5 y pegamos la url

{{< figure src="/images/madconvocala/guias_tacticas/icsx.png" >}}

Al abrir ICSx5 veremos un símbolo + abajo. Pulsamos y nos lleva a la pantalla de la imagen.
Pegamos la url en el campo destinado para ello y damos a la flecha.

### Paso 5 · Vamos a Calendar o Etar y configuramos.

Ahora ya nos aparecerá el calendario como opción para sincronizar en nuestros calendarios.
### Paso 6 · Fardar de conocer toda la agenda activista y cultural de Madrid

Ahora llevas mad.convoca.la contigo a todas partes. Difunde la palabra.
