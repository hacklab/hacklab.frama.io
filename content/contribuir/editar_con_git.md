+++
title = "Editar con git"
description = "Pasos para editar con git."
date = 2022-05-28T16:07:44+02:00
weight = 25
chapter = false
pre = "<b> </b>"
tags = [
    "Web estática",
    "Hugo",
    "Git"
]
toc = true
+++

Para seguir estos pasos **se requiere** conocimientos básicos del lenguaje de marcado Markdown y **una cuenta en [Framagit](https://framagit.org/users/sign_up)** [^1], que es la instancia de GitLab donde se aloja la página web. Además, se requieren conocimientos básicos de git, aunque si no los tienes, este puede ser un buen momento para aprender a utilizar este sistema de gestión de versiones.

[^1]: Las nuevas cuentas registradas en framagit requieren ser aprobadas por la moderacion de framagit, lo cual puede demorar varios dias. Si quieres colaborar modificando la web y no quieres esperar la aprobacion de tu cuenta de framagit puedes solicitar acceso a una cuenta comun del hacklab.

<u>Brevemente lo que vamos a hacer es:</u>

* Crear una cuenta de framagit.

* [Clonar el repositorio de la web (para tenerlo en local y poder editar) e intalar el submodulo que necesitamos.](#clonar-el-repositorio-de-la-webhttpsframagitorghacklabhacklabframaio)

* [Servir la web y editar.](#editar)

* [Fusionar los cambios que hicimos con la rama principal (en framagit) a traves de los issues y merge requests.](#fusionar-los-cambios)

### Editar un contenido o entrada ya creada

##### Clonar el [repositorio de la web](https://framagit.org/hacklab/hacklab.frama.io):

1. Se puede clonar mediante SSH, si previamente se ha subido la clave pública a framagit, o mediante HTTPS. Por seguridad, es recomendable realizarlo mediante SSH, debido a que la opción de HTTPS pide autenticarse con usuario y contraseña cada vez que se quiera realizar algún cambio. En cambio, mediante SSH y `ssh-add`, sólo será necesario introducir una vez la contraseña SSH y la autenticación es más fuerte al hacerse por claves público-privadas.    

```bash
# SSH
git clone git@framagit.org:hacklab/hacklab.frama.io.git

# HTTPS
https://framagit.org/hacklab/hacklab.frama.io.git
``` 

2. Actualizar el submódulo que contine el tema de Hugo. Al clonar el repositorio, la carpeta del módulo se descargará vacía. 

```
git submodule update --init
```

3. [Instalar Hugo](https://gohugo.io/getting-started/installing/)

    Si estás en Debian o Ubuntu, se recomienda instalar una versión > 0.104.1. Puedes seleccionar el fichero hugo_**extended**_XXX_linux-XXX.deb desde [releases](https://github.com/gohugoio/hugo/releases). Una vez descargado dirígete a la carpeta de Descargas y corre:

{{% notice style="warning" %}}
Es muy importante que se instale la versión **extended**, sino el tema elegido no funcionará.
{{% /notice %}}


```bash
sudo dpkg -i nombredelpaquete.
```
    
Para comprobar que la versión instalada es la correcta corre:
    
```bash
hugo version
```

Si usas Mac asumiendo que tienes [Homebrew](https://brew):

```bash
brew install hugo
```

Si estás con Windows y usas [Chocolatey](https://chocolatey.org/):

```bash
choco install hugo -confirm
```

#### Editar

1. **Servir la web** con `hugo serve` y abrir [http://localhost:1313]() en el navegador.

2. **Editar el fichero**. Para editar un fichero ya creado iremos a la carpeta `content/` donde veremos las secciones de la barra de navegacion. Elegiremos el fichero que queremos editar y haremos los cambios con nuestro editor de codigo preferido.

3. **Visualizar los cambios** en el navegador después de guardar el fichero modificado. **Comprobar que los cambios funcionan en local.**

#### Fusionar los cambios

Lo ideal es utilizar los [issues](https://framagit.org/hacklab/hacklab.frama.io/-/issues) y [merge requests](https://framagit.org/hacklab/hacklab.frama.io/-/merge_requests) para indicar qué cambio vamos a realizar en el código. Aunque al principio puede parecer un poco lioso, son buenas prácticas para la revisión de código y adminitración de tareas.

1. Primero abrimos un issue con un merge request asociado. En los issues planteamos que problema queremos resolver. Esto nos ayudará a focalizar y pensar la mejor solución al problema. Para ello, iremos a la ventana de los issues y crearemos uno nuevo con un nombre autodescriptivo. A continuación, seleccionaremos la opción de crear un merge request asociado y lo completaremos. Se habrá creado una nueva rama, que necesitaremos en los siguientes pasos. Por lo que la copiaremos y utilizaremos `git checkout -b <nombre-rama-creada-merge-request>` para editar sobre ella.

2. Después de comprobar que todo funciona, podremos ver los archivos modificados con `git status` y los cambios concretos con `git diff`. Para añadir los archivos que queramos actualizar al estado stage, ejecutamos `git add <nombre-archivo-a-actualizar>`, o `git add .` en caso de querer comitear todos los cambios realizados. Para pasar del estado stage al final, haremos un commit en la rama creada con `git commit -m "Mensaje descriptivo de los cambios"`. Para subir los cambios al repositorio origen usaremos `git push origin <nombre-rama-creada-merge-request>`. En este caso, se habrá creado un merge request que habrá que pasar del estado Draft al definitivo y aprobarlo para que se fusionen los cambios de la `<nombre-rama-creada-merge-request>` con la `main`. Si no hemos creado un merge request asociado, simplemente haremos un commit en la rama main y lo suberemos al repositorio de origen.

3. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.

### Crear una entrada nueva en la web

Previamente, es necesario completar los pasos de la sección anterior.

Pasos:

1. Abrir el proyecto con el editor de código preferido y con una terminal. Si queremos crear un nuevo capítulo o sección con el nombre `nuevo`, ejecutamos lo siguiente:

    ```zsh
    hugo new --kind chapter nuevo/_index.md
    ```

    Si simplemente queremos crear una nueva entrada, escribimos:

    ```zsh
    hugo new hugo/quick_start.md
    ```

2. Después, ya se podrá visualizar los cambios en el navegador después de guardar el fichero modificado.

3. Hacer un commit en la rama main y subirlo a la instancia de GitLab.

4. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.
