+++
title = "Aprendizaje"
description = "Grupos de aprendizaje cooperativo"
date = 2022-09-04T16:07:44+02:00
weight = 5
archetype = "section"
pre = "<b><i class='fa-solid fa-book-font'></i> </b>"
tags = [
]
toc = true
draft = false
+++

## Tabla de Contenidos

{{% children depth="4" /%}}
## Iniciativas

### Aprender a enseñar

Se propuso hacer una reunión con la gente de la Prospe y la Uni popular para
definir un plan de formación sobre cómo enseñar.

### Garantizar actividad de aprendizaje todos los miércoles

Queremos definir una hora fija en la que todos los miércoles se realizará una
actividad de aprendizaje.

Esta actividad puede ser desde apoyo uno a uno a resolver dudas, problemas y bloqueos
hasta sesiones de formación específicas.

Para tener una visión sobre los intereses de las personas del hacklab hicimos
este
[listado](https://cryptpad.disroot.org/sheet/#/2/sheet/edit/CbxTFYsc9OLBVRUGoUy-HBqn/).

### Crear un repositorio de documentación para ayudar al aprendizaje

Queremos armar en la wiki una biblioteca sobre cada tema, donde se encuentre una
guia e como ir aprendiendo y material (serian recursos pensados en cuanto a como
ir aprendiendo, el material recomendado para cada cosa, etc., no el material en
si porque para eso esta internet entero).

El sistema sería que cuando alguien esté interesada en introducirse en un tema,
vaya a los recursos y pregunte a una mentora sus dudas (habría un espacio para
esto en los encuentros de los miércoles), y cuando alguien quiera proponer un
tema lo anote en la tabla.

## Documentos importantes

* [Listado de intereses de la gente del
    hacklab](https://cryptpad.disroot.org/sheet/#/2/sheet/edit/CbxTFYsc9OLBVRUGoUy-HBqn/).
