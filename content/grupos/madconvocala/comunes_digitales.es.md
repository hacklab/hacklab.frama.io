+++
title = "Comunes digitales o cómo okupar los territorios digitales"
description = "Reflexiones sobre soberanía comunicativa y cómo okupar los territorios digitales"
date = 2023-05-05T16:07:44+02:00
weight = 10
chapter = false
tags = [
]
toc = true
+++

Un **Centro Social Autogestionado (CSA)** se organiza en torno a un espacio físico y un grupo de personas. Se entiende mejor si se piensa como un *verbo*, *desde la acción*, en un *contexto* y con una *visión compartida*.

En muchas ocasiones la práctica de CSA'r consiste en convivir (en) y defender (un) espacio donde diversos vínculos se cultivan y enredan resultando en la creación de nudos en la historia de nuestros barrios, pueblos... territorios.

En el contexto actual, donde las grandes corporaciones tecnológicas se están apoderando de los canales de difusión y actuando como agentes mediadores de los contenidos que distribuyen, nos encontramos con que la práctica fuera del espacio físico del CSA está totalmente subyugada a los territorios digitales que habitamos. ¿Cómo desplegar estrategias afines a nuestros valores que nos liberen de estas opresiones sistemáticas? 

Venimos a hablar(n)os de estas estrategias. Venimos a proponer que los CSA **okupen un espacio digital** para ayudar a liberar de muchas opresiones (ya sistémicas) a las personas que les dan vida.

## El bozal dentro del ordenador, en el código. ¿Soberanía comunicativa?

Imaginemos por un momento que sólo pudiéramos escribir en papel, que quisiéramos escribir un diario personal, o colectivo, algo que es importante para nosotras, para nuestra memoria. Imaginemos ahora que solo tenemos a nuestra disposición un cuaderno en el que escribir, peor aún, imaginemos que ese cuaderno no es nuestro ni va a estar en ningún momento en nuestra posesión. Imaginemos que "una autoridad" será quien determine cuándo podemos escribir en dicho cuaderno porque estará todo el tiempo a nuestro lado. No solo eso, también estará mirando lo que escribimos por lo que decidirá lo que podemos escribir y lo que no.

> **"Hola, me llamo Instagram y no quiero pezones en mi cuaderno."**

A este punto hemos llegado, los territorios digitales están totalmente dominados por las grandes corporaciones tecnológicas y eso, por desgracia, afecta en la forma en la que nos imaginamos a nosotras mismas y las formas en las que interactuamos (redes sociales, colegios, compra-venta...).

Queremos buscar un equilibrio entre las fuerzas de las que disponemos y las múltiples luchas a las que nos enfrentamos. Necesitamos soluciones transversales, que cambien nuestra práctica de forma que salgamos fortalecidas.

La cuestión central en este apartado es: **¿somos soberanas de nuestra comunicación?** ¿contamos con las estrategias y las tácticas adecuadas para contarnos nuestra historia sin tener que rendir cuentas a terceros?

En este punto no queremos limitarnos a pensar en clave digital. El entorno digital no es más que otro medio a través del cual comunicarnos y no pretendemos restarle importancia al resto, todo lo contrario. Lo que buscamos con estas propuestas es asegurarnos de que el medio digital no se apropie (de) y acabe dando forma a nuestro relato.

Y para ello queremos **problematizar** brevemente **la comunicación en entornos digitales**.
Con la metáfora anterior salen a la luz algunas características del modelo comunicativo en el que nos vemos inmersas:

- Los medios de difusión no nos pertenecen y como consecuencia *dependemos de plataformas digitales* para llegar a nuestras compañeras. Es interesante cómo nos vemos obligadas a adaptarnos incluso al formato impuesto; imagen, texto, video...    

- *Los contenidos son almacenados en ordenadores de grandes empresas* y no en los nuestros cuando permitimos que las interacciones (en territorios digitales) ocurran en las ya mencionadas plataformas. ¿Cuántos grupos de Facebook, perfiles de Instagram, grupos de Telegram contienen la memoria de nuestros colectivos? Y si mañana estas plataformas deciden cerrar nuestras cuentas, ¿qué haremos entonces? Pero no es un tema solo del futuro, ¿cómo lo hacemos ahora? quién no ha tenido problemas con cuentas de proyectos grandes, cómo gestionamos las credenciales, lo que se publica y lo que no, si la voz es colectiva o una individualidad. Todo este encorsetamiento está enraizado en el código y ya parece una opresión asumida.

## Imaginando vías de escape colectivas y autogestionadas

Desde [mad.convoca.la](https://mad.convoca.la) creemos en el encuentro, en el diálogo, queremos vernos más, conocernos mejor y esto no es una tarea fácil en el mundo en el que vivimos. Por ello creemos que un primer paso puede ser apropiarnos de una agenda. Una agenda en la que colocar las fechas destacadas, donde quepa la diversidad de las luchas y las **circustancias propias de cada una**. 

Pensamos en **una agenda como una herramienta** para gestionar el tiempo y somos conscientes de que en un mundo capitalista el tiempo es un recurso demasiado escaso. Cuando decimos agenda no pensamos en una agenda tal y como la imaginamos ahora, un cuaderno único, que gestiona "una autoridad". Necesitamos que cada colectivo pueda definir sus propios límites, que tenga soberanía sobre lo que apunta y cómo lo apunta. Tenemos las llaves para que esto cambie (contamos con personas que saben programar, con personas que han participado o participan en medios de comunicación, en movimientos por la soberanía comunicativa...), tal vez necesitamos ponerle un nombre para salir del marco de referencia actual. Esta nueva agenda no será un cuaderno, *será un común o no séra*. 

Proponemos **pensarla y vivirla como un común** porque necesitamos del encuentro de personas de diversos colectivos. Proponemos que sean; una asamblea, una carta de valores y un espacio (en este caso digital) los que pongan las normas de "nuestra" agenda. Aparecen, por tanto, conceptos que nos son familiares y que sí nos muestran las dificultades que puede acarrear una inciativa como esta. La gobernanza del proyecto es un tema central junto a la representatividad de la asamblea y la accesibilidad a la misma. Todo ello hablando de una iniciativa que lo que busca es acercar a los movimientos, por lo que debe tener capacidad de aglutinarlos.

Al mismo tiempo hablamos de la necesidad de no centralizar la comunicación por lo que esta agenda no es más que una herramienta complementaria que permite añadir redundancia a los canales de comunicación más locales propios de cada colectivo. Es un altavoz, pero no por ello nos debe forzar a duplicar esfuerzos de publicación ni confiar nuestra memoria a un ordenador ajeno al de nuestro colectivo (como ocurre en las redes sociales).

## Con vocación de convocar

Para poder llevar a cabo esta acción estratégica que nos permita poco a poco reforzar nuestra soberanía comunicativa, proponemos repensar nuestras tácticas digitales más allá de las pantallas:
    
mad.convoca.la se presenta como una **asamblea itinerante y abierta** (con base en el hacklab de la Ferroviaria), con vocación de escuchar las realidades comunicativas de nuestros movimientos, difundir nuevas prácticas de uso de las tecnologías digitales y acompañar en el proceso de transformación. 

Está presente en los territorios digitales como una agenda pública y federada de eventos activistas y culturales. Consiste en una web que replica los eventos de las páginas de colectivos sociales, permite publicar de forma anónima en caso de no disponer de infraestructura, cuenta con un perfil del fediverso y un canal de Telegram además de la posibilidad de suscribirse por RSS, sincronizar calendarios por ICS/ICAL o incrustar sus eventos en otras páginas a través de un componente web. 

Si os interesa empezar a hacer uso de la herramienta os invitamos a pasaros por una de nuestras asambleas para conocernos en persona y poder facilitaros cualquier información que necesitéis. O a contactar con nosotras a través de correo electrónico a <a href="mailto:mad.convocala<arroba>sindominio.net">`mad.convocala<arroba>sindominio.net`</a>.