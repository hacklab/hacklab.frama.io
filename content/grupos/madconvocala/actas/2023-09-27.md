+++
title = "Asamblea 2023-09-27"
description = "Acta de septiembre de 2023"
date = 2023-06-06T16:07:44+02:00
weight = 18
chapter = false
tags = [
]
toc = true
+++

Asamblea mad.convoca.la 27/09/2023 en el Hacklab de la Ferroviaria

El orden del día y el propio acta están juntos.

## 1. Ronda de presentación/pronombres

## 2. Actualización

2.1 Resumen de la última asamblea

2.2 Presentación de la herramienta en la Enredadera

2.3 Actualización Villana, Brecha, Horizontal

2.4 Se ha pasado por NoALaTala

## 3. Próxima asamblea

Tentativa para el miércoles 18/10/2023

## 4. Propuesta de mapeo

Se propone añadir una vista de mapa con los lugares donde se realizan eventos. Se ha desarrollado esta página: https://community-radar.com/pages/map.html en base a un taller de mapeo participativo que se hizo en la Enredadera. Se propone que las personas interesadas se organicen en un grupo de trabajo para desarrollar un plugin de gancio que habilite esta posibilidad.

También se comenta que en el espacio vecinal de arganzuela también hay interés por un mapeo.

## 5. Desarrollo del bot de telegram

5.1 subir eventos 

- de forma anónima está funcionando

- con autenticación está el desarrollo atascado, lo vamos a revisar en el hackmeeting

- el repo está en: [https://framagit.org/hacklab/publish-to-gancio-telegram-bot](https://framagit.org/hacklab/publish-to-gancio-telegram-bot) 

- se ofrece alguien de la asamblea para hostearlo

5.2 forward de eventos:

- otro bot de telgram para hacer fordward desde el canal de eventos publicados pricipal al canal de telegram del colectivo del evento. Le echamos un ojo en el hackmeeting.

## 6. Elaboración de guías de uso: 

Se están elaborando unas guías de uso y un fanzine para presentar mad.convoca.la.

## 7. Contribuciones a la página de convoca.la: 

Existe la página convoca.la que es la que centraliza las diferentes instancias, podemos hacer contribuciones para mejorarla

## 8. Talleres de cómo usar convoca.la: 

Se propone preparar un taller práctico de cómo usar gancio para la kafeta del hacklab para  difundir el uso de la herramienta.

## 9. Reunirse/Taller con bcn.convoca.la

- ¿Cómo contactar con ellos?

- ¿Cómo movieron el proyecto por ahí?

- ¿Habéis desarrollado materiales?

- ¿Los colectivos suben sus propios eventos?

- Si están interesados en colaborar con el desarrollo de los proyectos que llevamos (bots, mapeo...)?

## 10. Reparto de tareas del colectivo

10.1: Validar y contactar usuarias nuevas

10.2: Subir eventos

10.2.1: Subir los eventos al canal de "Subir eventos"

10.2.2: Subir a convocala estos eventos

10.3: Moderar eventos