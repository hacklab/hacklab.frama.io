module framagit.org/hacklab/hacklab.frama.io

go 1.22.8

require (
	github.com/Hanzei/hugo-component-osm v0.0.1 // indirect
	github.com/McShelby/hugo-theme-relearn v0.0.0-20241011141828-73672a72f133 // indirect
)
