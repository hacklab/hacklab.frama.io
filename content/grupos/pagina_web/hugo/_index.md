+++
title = "¿Cómo crear una página web con Hugo?"
description = "Aprende a crear una página web con Hugo"
date = 2022-06-20T12:17:37+01:00
weight = 20
archetype = "section"
pre = "<b> </b>"
+++

# Hugo

En esta sección o capítulo se explican los primeros pasos para crear y desplegar una página web estática generada con Hugo en GitLab pages.
