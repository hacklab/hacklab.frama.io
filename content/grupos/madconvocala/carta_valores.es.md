+++
title = "Carta de valores"
description = "Principios y valores de mad.convoca.la"
date = 2023-05-05T16:07:44+02:00
weight = 20
chapter = false
tags = [
]
toc = true
+++

De momento hemos adoptado la carta de valores de [bcn.convoca.la](https://bcn.convoca.la):

## Comunicación y organización

- La okupación digital de espacios de **comunicación y autoorganización**.
- Apoyo a la **difusión comunitaria** de los movimientos sociales.
- Por la comunicación entre colectivos, **por entrelazarnos más y pisarnos menos**.
- Para **facilitar la planificación** personal de las activistas.

## Tecnología

- **Autogestionada**: que devuelve la gestión de contenidos a la comunidad y colectivos los cuales generan estas propuestas, tanto culturales y sociales.
- **Independiente (que no excluyente)**: una *respuesta alternativa* a las plataformas como Facebook, Instagram, Telegram o Twitter. Que hacen vigilancia masiva, censura política, manipulación social (publicidad), y control de contenido que es poco transparente y se ha comprobado que favorece a los poderes económicos.
- **Respetuosa con la privacidad**: minimización de datos recogidos, identidades colectivas, publicaciones anónimas.
- **Autónoma**: utilizamos una herramienta de código libre y colaborativa ([Gancio](https://gancio.org/)), alojada en servidores radicales ([Sin Dominio](https://sindominio.net/)).

## Principios

Tenemos una óptica **interseccional**. Todas las luchas son necesarias y es necesario **unir fuerzas** en base al respeto y el aprendizaje mutuo, reconociendo los propios privilegios, compartiéndolos, renegando, y luchando contra los privilegios de otros, y por la justicia social y la equidad.

Queremos contribuir al trabajo que están realizando colectivos implicados en:

- Antirracismo, derechos de personas migrantes, lucha anticolonial
- Transfeminismos
- Movimiento LGBTIQ+
- Sindicatos de Vivienda, antidesahucios y de barrio
- Centros sociales, ateneos (okupados, autogestionados, populares, libertarios)
- Sindicalismo combativo
- Anticapitalismos
- Ecologismos y animalismo
- Agroecología y movimientos rurales
- Abolicionismo de la represión, lucha anticarceria
- Defensa y transformación de servicios públicos: educación, sanidad, transporte...
- Movimiento cooperativo

No queremos contribuir a variantes de estas luchas absorbidas por el capitalismo (liberales, mercantilizadas) o por el Estado (institucionalizadas)

- feminismo liberal
- feminismo transexclusivo o putofóbico (terf/swerf)
- sindicatos amarillos
- partidos políticos
- instituciones de la administración pública o su órbita cómplice
- racismo o clasismo asistencialista
- extrema derecha
- capitalismo verde, ecofascismo
- movimientos "apolíticos" instrumentalizados por la derecha