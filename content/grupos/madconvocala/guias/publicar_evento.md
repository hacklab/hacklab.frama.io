+++
title = "¿Cómo publicar un evento?"
description = "Guía de cómo publicar un evento en mad.convoca.la"
date = 2024-01-14T18:07:44+02:00
weight = 15
chapter = false
tags = [
]
toc = true
+++

En primer lugar, la herramienta se **centra en el contenido**, no en las personas. En ninguna parte de gancio aparece la identidad de quien ha publicado un evento, ni siquiera bajo un apodo, ni siquiera para les administradores (excepto en la base de datos). Esta no es una plataforma amiga del ego.

En segundo lugar, se tiene la filosofía de que les **visitantes son lo primero**. No queremos que una persona registrada obtenga más funciones que une visitante cualquiera. No es necesario registrarse, excepto para publicar eventos aunque incluso también se puede publicar un evento anónimamente.

En tercer lugar, cada [instancia de gancio](https://gancio.org/instances) tiene su propia carta de valores o principios para moderar los eventos. En [mad.convoca.la](https://mad.convoca.la) tenemos [esta carta de valores](https://mad.convoca.la/about). Será necesario cumplir los principios expuestos para subir eventos.

Por lo tanto, existen **dos formas de publicar** en [mad.convoca.la](https://mad.convoca.la): **anónimamente o con una cuenta verificada**. 

## Eventos anónimos

No es necesario tener una cuenta para subir una convocatoria. Las convocatorias publicadas sin cuenta serán revisadas por nosotres. Intentamos publicarlas enseguida, pero somos voluntaries: si tienes mucha prisa, cuélgala con más antelación la próxima vez.

Los pasos a seguir para publicar anónimamente son los siguientes. Empezamos clickando en el botón [Nuevo Evento](https://mad.convoca.la/add) que nos redireccionará a un formulario como el siguiente. 

{{< figure src="/images/madconvocala/guias_tacticas/add_event.png" >}}

El cual contiene los campos obligatorios de Título, Lugar y Fecha. Primero rellenamos el título, seguido del espacio donde vaya a ocurrir. Por defecto, se detectará si el lugar ha sido añadido previamente. En caso de no estar el lugar registrado, configuraremos la ubicación mediante el botón de ajustes. Buscaremos la dirección en [Openstreetmaps](https://www.openstreetmap.org) y copiaremos la latitud y longitud en los campos correspondientes. Es importante verificar que la ubicación es correcta antes de añadirla. Pondremos un nombre a la dirección y seguiremos rellenando el formulario.

{{< figure src="/images/madconvocala/guias_tacticas/add_place.png" >}}

En la sección del calendario, seleccionaremos el tipo de evento según la frecuencia (Normal, Más días o Recurrente) y una fecha. Normal sirve para un día concreto o actividades puntuales. También se pueden publicar como una sola actividad actividades que duren más de un día, como jornadas, fiestas, acampadas, eventos o encuentros. Recurrente sirve para que se repita un evento con una frecuencia determinada. Las actividades periódicas como talleres, asambleas, deportes, etc, también tienen cabida, pero pedimos responsabilidad de ir actualizándolas para conseguir que nadie se tope con una puerta cerrada. 

Una vez seleccionada el tipo de fecuencia, tendremos que elegir la hora de comienzo. La hora de finalización es opcional.

{{< figure src="/images/madconvocala/guias_tacticas/add_date.png" >}}

Para añadir una descripción, copiamos y pegamos en el campo de texto que se muestra en la siguiente imagen. El texto soporta distintos formatos y estilos de edición. Esta descripción se acortará si es muy larga al enviarse por el canal de telegram [t.me/madconvocala](https://t.me/madconvocala).

{{< figure src="/images/madconvocala/guias_tacticas/add_description.png" >}}

Por otro lado, podemos añadir el cartel del evento en la sección de media. Si no tenemos cartel, podemos crear uno rápidamente con [la herramienta de creación de posters](https://hacklab.frama.io/grupos/madconvocala/poster/index.html). Adjuntaremos la imagen del cartel y editaremos la previsualización del mismo para que quede centrado. Guardaremos y comprobaremos que se visualiza bien.

{{< figure src="/images/madconvocala/guias_tacticas/add_media.png" >}}

El campo de etiquetas es libre, pero intentamos utilizar etiquetas existentes equivalentes para facilitar búsquedas futuras. (por ejemplo: #medioambiente, #arte, #LGTBIA+, #vivienda, #ecologismo, #concierto, #kafeta). Intentamos que sigan este formato:

1. Formato del evento (charla, taller, concierto, manifestación, presentación, …)
2. Temática o lucha (antiracismo, feminismo, vivienda, …)
3. Barrio o distrito
4. Hashtags que tenga el cartel o el texto que acompaña
5. Asistencia (no mixto, espacio para peques)
6. Colectivo/s que organiza/n

{{< figure src="/images/madconvocala/guias_tacticas/add_tag.png" >}}

Por último, le daremos a enviar para que las moderadores revisen que cumpla con los principios. Somos voluntarias e intentaremos subir los eventos lo antes posible pero podremos tardar varios días en aceptarla. 

## Cuenta registrada

El proceso es el mismo que al subir eventos anónimamente pero teniendo un registro previo y habiendo iniciado sesión. El proceso de registro funciona por confianza y se verifica que la persona solicitante es quien dice ser.

## Importar ICS

Una opción para importar un evento a Gancio es mediante el estándar para el intercambio de información de calendarios iCalendar, también conocido como iCal cuyas extensiones son .ics. Al crear un nuevo evento podremos importar el archivo desde una dirección web o subiendo el archivo .ics.

{{< figure src="/images/madconvocala/guias_tacticas/import_ICS.png" >}}
