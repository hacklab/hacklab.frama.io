+++
title = "Desarrollo de páginas web"
description = "Grupo de trabajo encargado de desarrollar la página web"
date = 2022-06-20T12:17:37+01:00
weight = 20
archetype = "section"
pre = "<b><i class='fa-solid fa-display-code'></i> </b>"
+++

# Página Web del HackLab Unipopular

En esta sección se presentan los recursos discutidos en el grupo de trabajo de la página web del HackLab Unipopular. 