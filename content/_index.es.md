+++
author = "HackLab"
title = "HackLab UniPopular"
description = "HackLab Uni Popular"
weight = 1
draft = false
date = 2022-05-28T16:07:44+02:00
archetype="home"
+++

![Logo](logos/logo_150x150.jpg?height=150&classes=shadow)

## ¿Quiénes somos?

Un grupo de personas que formamos un espacio interdisciplinar de aprendizaje cooperativo y cacharreo tecnológico utilizando software libre.

## ¿Cómo encontrarnos?

Puedes contactar con nosotras a través del grupo Interhacklabs, en Telegram:

{{% button href="https://t.me/+EmWc6-CNLPJlMTc0" style="secondary" icon="fab fa-telegram" iconposition="right" %}}¡Únete a nuestro telegram!{{% /button %}}

O bien acercarte a cualquiera de los tres espacios que están activos actualmente:

- [0day](https://0day.lol) (e.s.l.a. Eko)
- Teknokasa (CSO La Rosa)
- La raíz (CSOA La Enre)

{{< hacklab-events >}}

## Tabla de Contenidos

{{% children description="true" depth="4" /%}}