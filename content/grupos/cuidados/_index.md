+++
title = "Grupo de Cuidados"
description = "Grupo de cuidados"
date = 2022-09-04T16:07:44+02:00
weight = 10
chapter = false
pre = "<b><i class='fa fa-hand-holding-hand'></i> </b>"
tags = [
]
toc = true
+++


( A completar con el manifiesto del grupo )

Qué rasgos definen un espacio como seguro:

- Libre de violencias de todo tipo
- No ocurren comentarios (micro)machistas, (micro)xenófobos, ni micro(homófobos)...
- Siempre abierto a la escucha
- ... pendiente definir más

# Decisiones

## Conseguir que el hacklab sea un espacio seguro

Dar respuesta a comentarios, comportamientos o actitudes que pueden hacer peligrar el
espacio seguro interviniendo de manera constructiva, positiva y pedagógica para
la persona que la recibe y que sirva para evitar otras situaciones similares en
el futuro.

## Generar un espacio de cuidados en cada encuentro

Generar un espacio cada encuentro de entre 15 y 20 minutos de crítica
y autocrítica para plantear nuestros sentires y poder ir trabajando de a poco
nuestra perspectiva en torno a la crítica y al como gestionarla (a nivel
personal y colectivo). La idea es que se haga cada sesión para que sea una
construcción continua y constante pero sin que se haga pesado (en sí y el que
alguien tenga que preparar dinámicas y etc.), pero que en caso de que lo
sintamos necesario, hacer una reunión larga para tratar algo en particular.

Estaría pensado a las 20, e intentaríamos garantizarla entre todas, es decir,
que quien se dé cuenta de que es el horario recuerde que es el momento de
cuidados.

# Ideas en el aire

- Escribir un manifiesto que guíe al grupo.
- Tratar de que las personas que llegan al espacio (nueva o no) se sientan
    cómodas.
- Necesitamos una actitud activa para que el hackLab sea realmente un espacio
    seguro.
- Hay actitudes que pueden generar incomodidades y romper ese "espacio seguro".
- Que las personas del hacklab puedan escribir de forma anónima temas a tratar.
- Pedir consejo o apoyo a gente de la ferro, asambleas feministas, otros
    colectivos.
    - Podríamos tirar de grupos de hombres y empezar con trabajo propio,
        requiere un esfuerzo extra, e incluso se podría montar un grupo de
        "masculinidades" fuera del hacklab.
- ¿Cómo se puede hacer seguimiento cuando hay gente que se va del colectivo? ¿Puede ser que se haya sentido incómoda?
- Buscar equilibrio entre cuidados / intervención - (no convertir el hacklab en un espacio de terapia)
