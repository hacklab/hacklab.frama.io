+++
title = "Funcionamiento y organigrama"
description = "¿Cómo funcionamos? ¿Qué partes tiene el proyecto?"
date = 2023-05-05T16:07:44+02:00
weight = 15
chapter = false
tags = [
]
toc = true
+++

## Funcionamiento

- **Publicaciones anónimas**. No es necesario tener una cuenta para subir una convocatoria. Las convocatorias publicadas sin cuenta serán revisadas por nosotras. Intentamos publicarlas enseguida, pero somos voluntarias: si tienes mucha prisa, cuélgala con más antelación la próxima vez.

- **Cuentas por colectivos afines**. Si compartís la [carta de valores]({{% ref "/grupos/madconvocala/carta_valores" %}} "Edita con interfaz gráfica"), podéis escribirnos a  <a href="mailto:mad.convocala@sindominio.net">`mad.convocala<arroba>sindominio.net`</a> y os explicaremos cómo empezar a publicar. Evidentemente, nos reservamos la posibilidad de deciros que no si creemos que no somos compatibles.

  Dentro de nuestras capacidades, invitaremos a colectivos que ya conozcamos o con los que tengamos relaciones de confianza y compañerismo.

- **Etiquetas**. El campo de etiquetas es libre, pero intenta utilizar etiquetas existentes equivalentes para facilitar búsquedas futuras. (por ejemplo: #medioambiente, #arte, #LGTBIA+, #vivienda, #ecologismo, #concierto, #kafeta). Intentamos que sigan este formato:
  - Formato del evento (charla, taller, concierto, manifestación, presentación, …)
  - Temática o lucha (antiracismo, feminismo, vivienda, …)
  - Barrio o distrito
  - Hashtags que tenga el cartel o el texto que acompaña
  - Asistencia (no mixto, espacio para peques)
  - Colectivo/s que organiza/n

- **Actividades puntuales, periódicas, o jornadas**. Las actividades periódicas como talleres, asambleas, deportes, etc, también tienen cabida, pero pedimos responsabilidad de ir actualizándolas para conseguir que nadie se tope con una puerta cerrada.

  También se pueden publicar como una sola actividad actividades que duren más de un día, como jornadas, fiestas, acampadas, eventos o encuentros.

## Organigrama de mad.convoca.la

- **mad.convoca.la**: Colectivo de activistas y militantes que da vida a una agenda activista y cultural pública y federada, surge desde el hacklab de la Ferroviaria donde podéis encontrarnos los miércoles a partir de las 18:30-19.

- **convoca.la**: Iniciativa propulsada por sindominio.net para impulsar el uso de Gancio como herramienta digital para la autoorganización de movimientos sociales. Gestionan el alojamiento de la herramienta facilitando un despliegue propio para los territorios que lo pidan.

- **sindominio.net**: "Sindominio es un servidor de internet autónomo y autogestionado por una comunidad de seres que apuestan por formas de funcionamiento digital horizontales y seguras. Sindominio nació en 1999, se organiza de manera asamblearia y hoy sigue siendo un espacio para el aprendizaje mutuo y el empoderamiento tecnológico. Sindominio provee a activistas de herramientas y recursos en internet como correo electrónico, listas de correo, mensajería instantánea y páginas web, entre otras."

- **Gancio**: Aplicación web de código libre que procura ser una agenda compartida para comunidades locales. El proyecto nace en Italia en el [Hacklab Underscore](https://www.autistici.org/underscore/) que actualmente mantiene el código. Funcionalidades relevantes:

  - *Enfocada en el contenido no en las personas*: la identidad de las personas que publican en Gancio no aparece en ningún lado, ni siquiera bajo un pseudónimo ni siquiera a las usuarias administradoras (excepto en la base de datos). Esta no es una plataforma que fomente el ego en ese sentido.

  - *Visitas primero*: no queremos las personas con una cuenta tengan a su disposición más funcionalidades que una persona que está visitando la página sin darse de alta. Es más, no queremos que las usuarias se registren excepto en caso de que quieran publicar un evento, para lo cual también existe una forma anónima y sin registro de hacerlo.

  - *Eventos anónimos*: de manera opcional, una persona visitante de la web puede crear un evento sin registrarse (una administradora deberá validar dicho evento)

  - *No nos importa dar el bombazo informativo*: por lo que exportamos nuestros eventos de múltiples formas, via feeds de RSS, via  ics individual o global, permitiendo crear iframes para embeber uno o varios eventos en tu propia página web, via AP, microdata y microformat.

  - Interfaz de usuarix (UI) intuitiva.

  - Se pueden crear eventos que duren varios días

  - Se pueden crear eventos recurrentes

  - Filtrado de eventos a través de un sistema de etiquetas

  - Exportación con filtros incluida de RSS e ICS

  - Posibilidad de embeber eventos con componentes web e iframes

  - Filtro de eventos por tags o lugares.

  - ¡Comparte, guarda y comenta eventos desde el fediverso!

  - Múltiples opciones de configuración (tema claro y oscuro, abrir o cerrar el registro de usuarias, habilitar eventos recursivos...

  - Licencia AGPL-3.0 Licence.
