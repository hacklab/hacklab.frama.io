+++
title = "Hacking"
description = "Hacking"
date = 2022-09-04T16:07:44+02:00
weight = 10
chapter = false
pre = "<b> </b>"
tags = [
]
toc = true
draft = true
+++

## Decisiones
Se van a dar talleres prácticos de seguridad. 
Los primeros constan de dos partes.
Una para montar un laboratorio para hacer pentesting avanzado, y para aprovechar la infra del laboratorio, se dará algo mas genérico de introducción al hacking.

## Tareas

- Preparar los talleres de pentesting.

## Congresos

| **Congreso**     | **Lugar** | **Fecha**                        | **Web**                      | 
| :--------------- | :-------- | :------------------------------- | :--------------------------- | 
| **NoConName**    | Barcelona | 23, 24 y 25 de Noviembre de 2022 | https://www.noconname.org/   | 
| **Navaja Negra** | Albacete  | 10, 11 y 12 de Noviembre de 2022 | https://www.navajanegra.com/ | 
| **BitUp**        | Alicante  | 28 y 29 de Octubre de 2022       | https://bitupalicante.com/   | 
| **Asturcon**     | Asturias  | 23 y 24 de Septiembre de 2022    | https://asturcon.tech/       | 