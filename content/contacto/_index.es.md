+++
title = "Contacto"
description = "¿Quieres conocernos?"
date = 2022-09-04T16:07:44+02:00
weight = 25
archetype = "section"
pre = "<b><i class='fas fa-envelope'></i> </b>"
tags = [
]
toc = true
+++

Todos los miércoles de 18h a 21h en la Ferroviaria (Plaza Luca de Tena, 7)

{{% button href="https://t.me/+EmWc6-CNLPJlMTc0" style="blue" icon="fab fa-telegram" %}}¡Únete a nuestro telegram!{{% /button %}}

O escribe cualquier duda al siguiente correo electrónico: <a href="mailto:hacklab-ferro@riseup.net">`hacklab-ferro<arroba>riseup.net`</a>.

Pásate a conocernos cualquier día que haya algún evento del hacklab :)

{{< hacklab-events >}}

{{< openstreetmap mapName="csoa-la-ferroviaria_985641" miniMap=true >}}