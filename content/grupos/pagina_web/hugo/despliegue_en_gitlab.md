+++
title = "Despliegue en Gitlab pages"
description = "¿Sabes cómo desplegar un proyecto en GitLab pages?"
date = 2022-06-20T16:07:44+02:00
weight = 10
chapter = false
pre = "<b> </b>"
tags = [
    "Web estática",
    "Hugo",
    "GitLab pages"
]
toc = true
+++

# ¿Cómo desplegar el proyecto en GitLab pages?

1. Configuramos la integración y despliegue contínuo creando el archivo `.gitlab-ci.yml` con la plantilla de Hugo. Si realizamos esto desde GitLab, en el contenido de la plantilla de GitLab, será necesario modificar la rama master por main.

    ```yaml
    # This file is a template, and might need editing before it works on your project.
    ---
    # All available Hugo versions are listed here:
    # https://gitlab.com/pages/hugo/container_registry
    image: registry.gitlab.com/pages/hugo:latest

    variables:
      GIT_SUBMODULE_STRATEGY: recursive

    test:
      script:
        - hugo
      except:
        - main

    pages:
      script:
        - hugo
      artifacts:
        paths:
          - public
      only:
        - main
    ```

2. Modificar la URL base que tiene esta estructura `baseURL = "https://<gitlab-user>.gitlab.io/<project-name>/"`.

3. Habilitar el acceso a todos los públicos a [GitLab](https://docs.gitlab.com/ee/user/project/pages/pages_access_control.html). Navegar en los ajustes del proyecto de GitLab y expandir Visibility, project features, permissions > Pages > Everyone.
