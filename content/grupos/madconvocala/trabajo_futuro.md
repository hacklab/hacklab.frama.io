+++
title = "Trabajo pasado, presente y futuro"
description = "¿Qué se necesita para crear una agenda comunitaria? Aquí dejamos un boceto de nuestro camino"
date = 2023-05-05T16:07:44+02:00
weight = 35
chapter = false
tags = [
]
toc = true
+++

{{< toc >}}

## Trabajo presente

### Automatización de ingesta de eventos

Para hacer más agradable el proceso de subir eventos estamos trabajando en las siguientes mejoras:

#### Sincronización de eventos con wordpress

Queremos volcar los eventos de las páginas de los colectivos y centros sociales a mad.convoca.la. Se hizo un listado rápido y la mayoria usaban wordpress.

Para conseguir la sincronización estamos explorando estas dos vías:

- Existe un plugin oficial de [Gancio](https://framagit.org/les/gancio/-/tree/master/wp-plugin) que se instalaría en los wordpress de los colectivos que subiría los eventos en convócala a traves de un token. Lo probamos y no funcionaba, pero le queríamos dar otra vuelta a ver si lo podíamos arreglar.
- [Hacer un programa escrito en Python de sincronización.](#hacer-un-programa-escrito-en-python-de-sincronización)

##### Usar el plugin de Gancio de wordpress

Hemos conseguido que funcione pero aún tiene errores, estamos trabajando para que funcione más 

##### Hacer un programa escrito en Python de sincronización. 

De momento esta vía está parada ya que parece que el plugin de wordpress funciona. Dejamos los pasitos escritos por si en el futuro queremos retomarla.

Pasitos:

- Subir eventos nuevos desde las webs de los colectivos a mad.convoca.la
  - Extraer los eventos de las webs de los colectivos
    - [ ] Hacer listado de las webs de las que queremos extraer los eventos
    - [ ] Seleccionar la que nos parezca más interesante
    - [ ] Analizar esta web para ver si tiene API o si hay que scrapearla
    - [ ] Crear el programa que de la información disponible de la web extraiga los datos
      - [ ] Acordar el esquema del modelo de datos (qué queremos guardar de cada evento y qué tipo tiene cada propiedad)
        - [ ] Analizar el modelo de datos de [gancio](https://demo.gancio.org/api/events) para hacerlo compatible
      - [ ] Crear el código que traduzca la información de origen al esquema de datos acordado
      - [ ] Crear el código que guarde esos datos
        - [ ] Decidir con qué formato se va a guardar
        - [ ] Decidir dónde se va a guardar
  - Introducir los eventos en mad.convoca.la
    - [ ] Ver cómo hacer peticiones autenticadas a [la api](https://framagit.org/les/gancio/-/blob/master/docs/.api.md)
      - [ ] Descubrir cómo generar el token de las peticiones
      - [ ] Descubrir cómo usar el token para hacer las peticiones
    - [ ] Extraer los eventos existentes en mad.convoca.la: Hacer petición a nuestra API para extraer los eventos. 
    - [ ] Comparar la información de los eventos con los que ya hayamos extraído de las páginas de los colectivos
      - [ ] Investigar si es posible hacer actualización de los eventos a través de la API
        - [ ] Si no lo es, abrir issue preguntando al desarrollador por la funcionalidad
      - [ ] Asignar un método u otro (POST/PUT) en función de si el evento existe o no
      - [ ] Hacer el código que haga la subida

- Actualizar eventos que hayan sido modificados en el origen

#### Automatización de ingesta de eventos desde telegram

Estamos haciendo un bot interactivo de telegram para subir eventos.
  También se pueden publicar como una sola actividad actividades que duren más de un día, como jornadas, fiestas, acampadas, eventos o encuentros.

## Trabajo futuro
    
- Guías tácticas para la soberanía comunicativa y tecnológica
    - Cómo publicar eventos en la web de tu colectivo y mandarlos automáticamente a Gancio
    - Cómo publicar de forma anónima en Gancio
    - Cómo sincronizar eventos de Gancio en tus calendarios personales
    - Todo en un canal de Telegram o en una cuenta del fediverso
    - Cómo publicar en un espacio propio y llegar a las redes sociales privativas

- [Organigrama de mad.convoca.la](#organigrama-de-mad.convoca.la)
- Estatutos de la asamblea
- Itinerario de la asamblea
- [Carta de valores de mad.convoca.la](#carta-de-valores-de-mad.convoca.la)
- [Automatización de ingesta de eventos](#automatización-de-ingesta-de-eventos)