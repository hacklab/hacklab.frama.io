+++
title = "Charter of principles"
description = "Principles and values of mad.convoca.la"
date = 2023-05-05T16:07:44+02:00
weight = 20
chapter = false
tags = [
]
toc = true
+++

For the time being, we have adopted the charter of values of [bcn.convoca.la](https://bcn.convoca.la):

## Communication and organization

- Digital squatting of **communication and self-organization** spaces.
- Support for **community broadcasting** of social movements.
- For communication between collectives, **for more interlinking and less stepping on each other's toes**.
- To **facilitate the personal planning** of activists.

## Technology

- **Self-managed**: which returns content management to the community and collectives that generate these proposals, both cultural and social.
- **Independent (but not exclusive)**: an *alternative response* to platforms such as Facebook, Instagram, Telegram or Twitter. Which do mass surveillance, political censorship, social manipulation (advertising), and content control that is not very transparent and has been proven to favor economic powers.
- **Privacy-friendly**: minimization of collected data, collective identities, anonymous publications.
- **Autonomous**: we use an open source and collaborative tool ([Gancio](https://gancio.org/)), hosted on radical servers ([Sin Dominio](https://sindominio.net/)).

## Principles

We have an **intersectional** perspective. All struggles are necessary and it is necessary to **join forces** on the basis of respect and mutual learning, recognizing one's own privileges, sharing them, denying them, and fighting against the privileges of others, and for social justice and equity.

We want to contribute to the work being done by collectives involved in:

- Anti-racism, migrant rights, anti-colonial struggle.
- Transfeminisms
- LGBTIQ+ movement
- Housing, anti-eviction and neighborhood unions
- Social centers, atheneums (squatted, self-managed, popular, libertarian)
- Combative trade unionism
- Anti-capitalism
- Environmentalism and animalism
- Agroecology and rural movements
- Abolitionism of repression, anti-carceration struggle
- Defense and transformation of public services: education, health, transport...
- Cooperative movement

We do not want to contribute to variants of these struggles absorbed by capitalism (liberal, commodified) or by the State (institutionalized).

- liberal feminism
- transsexual or putophobic feminism (terf/swerf)
- yellow unions
- political parties
- institutions of public administration or their complicit orbit
- racism or welfarist classism
- extreme right
- green capitalism, eco-fascism
- "apolitical" movements instrumentalized by the right wing