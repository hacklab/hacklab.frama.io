+++
title = "Distintas formas para editar esta web"
description = "Guías no técnicas y técnicas para modficar la web."
date = 2022-05-28T16:07:44+02:00
weight = 15
chapter = false
pre = "<b> </b>"
tags = [
    "Web estática",
    "Hugo",
    "GitLab"
]
toc = true
+++

- [**Editar desde la interfaz gráfica de una instancia de GitLab**]({{% ref "/contribuir/editar_desde_gitlab" %}} "Edita con interfaz gráfica"). Se requiere conocimientos básicos del lenguaje de marcado Markdown y una cuenta en la instancia de GitLab donde se aloja la página web.

    - Un contenido o entrada ya creada. 

    - Crear una entrada nueva en la web. No se necesitan conocimientos técnicos.

- [**Editar con git**]({{% ref "/contribuir/editar_con_git" %}} "Edita con git"). Se requiere conocimientos básicos del lenguaje de marcado Markdown y una cuenta en la instancia de GitLab donde se aloja la página web. Además, se requieren conocimientos de git y quitarse el miedo a usar la terminal (aunque también existen interfaces gráficas para git :) ).

    - Un contenido o entrada ya creada.

    - Crear una entrada nueva en la web.

- **Editar desde el móvil con Termux y Markor**.  Se requiere conocimientos básicos del lenguaje de marcado Markdown y una cuenta en la instancia de GitLab donde se aloja la página web. Además, se requieren conocimientos de git y no tener miedo a una terminal. 