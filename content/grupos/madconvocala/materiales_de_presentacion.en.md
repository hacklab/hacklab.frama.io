
+++
title = "Presentation materials"
description = "How to transmit the project to the collectives"
date = 2023-06-01T16:07:44+02:00
weight = 30
chapter = false
tags = [
]
toc = true
+++

Depending on the context in which we are to present the mad.convoca.la project to the collectives there are different formats that we can use:

- [Fanzine](#fanzine)
- [General dissemination message](#general-dissemination-message)
- [Contact message with collectives when we upload an event of theirs](#contact-message-with-collectives-when-we-upload-an-event-of-theirs)
- [Presentation at an assembly point](#presentation-at-an-assembly-point)
  - [Presentation of the project](#presentation-of-the-project)
  - [Presentation of the site](#presentation-of-the-site)
  - [Presentation of the collective](#presentation-of-the-collective)
- [Complete presentation of the project](#presentación-completa-del-proyecto)

## Fanzine

We present you our fanzine about mad.convoca.la!
In this publication we have synthesized the what and why of our Gancio instance. The idea is to make it known to different collectives so that they know about its existence and its capabilities. Feel free to spread it :)

{{< embed-pdf url="/js/madconvocala/Fanzine_madconvocala.pdf" >}}

## General dissemination message

You can forward [this message](https://t.me/madconvocala/113) otherwise you can copy and paste [the text from the main project page](https://hacklab.frama.io/grupos/madconvocala/mad.convoca.la/).

## Contact message with collectives when we upload an event of theirs

To find the list of groups we have contacted visit [this pad](https://pad.riseup.net/p/convocala-contacto-colectivos-keep), there you can also find the most updated version of the message to be sent.

```
Hi @CollectiveName, 

We just uploaded your event to https://mad.convoca.la. We hope you don't mind 😜, if not, let us know and we'll delete it :). You can see it here: 
    
<<Link to the event>>

Let us know if you see any improvement in the event data.

We are the collective mad.convoca.la (http://mad.convoca.la/) and we are promoting a political and countercultural agenda in Madrid. We watch over the digital squatting of communication and self-organization spaces. So we try to support community outreach to the events of social movements.

If you want to contribute, you can start uploading your events or spread the web. Word of mouth is always the best strategy. We also invite you to our online or traveling assembly open to collectives.

To know more about the project, here you have more information: https://hacklab.frama.io/grupos/madconvocala and https://mad.convoca.la/about.

Do not hesitate to tell us about any doubt or improvement you may have,
Best regards
```

Optional:

```
We have also seen that you have a calendar on the web with Wordpress. There is a Wordpress extension that would allow you to upload the events automatically to mad.convoca.la. If you are interested we can help you to install it.
```

## Presentation at an assembly point

Useful for when we present the project in the assembly of the collective to contact, or as an introduction if there are new people in the assembly of mad.convoca.la. The idea is that it should not take more than 10 minutes.

### Presentation of the project

We are a collective that is creating an **autonomous political and cultural agenda**, which wants to contribute to **disseminate and coordinate public actions** of social and political collectives in **Madrid**. To carry out the project we use two tools [a web page](#presentation-of-the-site) and [the collective](#presentation-of-the-collective).

### Presentation of the site

On [the website](https://mad.convoca.la) you can view and upload events in a friendly and intuitive way. As it is specifically made for this purpose it has very interesting functionalities such as:

- **Dissemination automation**. The page has mechanisms to automatically propagate information when an event is uploaded or edited. It is able to publish events on:

  - [Telegram channels](https://t.me/madconvocala).
  - [The collective's page](https://hacklab.frama.io/en/agenda/).
  - [The activists' calendar](https://mad.convoca.la/feed/ics?show_recurrent=true).
  - [In free social networks](https://mastodon.social/@madrid@mad.convoca.la).
  - [In RSS readers](https://mad.convoca.la/export).

- **Ease of creation of events**. Through:

  - [The website itself](https://mad.convoca.la/add).
  - The Wordpress plugin. 
  - The telegram bot. 

  You have the following aids when uploading an event:

  - Autocomplete tags and places.
  - Warning about events already convened to avoid stepping on our toes.
  - Graphical date and time selectors.
  - Selection of image fragment to display.

- **Easy visualization of events**. Through:

  - Main page with summary of all events.
  - Filtering by neighborhoods, places, tags or dates.
  - Specific page for each event.
  - See on a map where the event is going to be.
  - Creating routes on foot, by bike or car to know how to get there.

- **Anonymous creation of events**. In case you don't want to create an account or be associated with the event.

- **Recover communicative sovereignty**. The page is hosted in radical servers ([Sin Dominio](https://sindominio.net/)), we use a free and collaborative code tool ([Gancio](https://gancio.org/)) and an assembly collective ([Convócala Madrid](https://hacklab.frama.io/grupos/madconvocala/mad.convoca.la/)) decides what is published and what is not. So we do not depend on capitalist networks and filters to disseminate our events.


### Presentation of the collective

From the promoter collective we have established a series of [principles](https://hacklab.frama.io/en/grupos/madconvocala/carta_valores/#principios) that events must comply with in order to be admitted. However, in the day-to-day management of the agenda, debates and political decisions arise that we do not feel we have the power to make alone. For example:

- Which groups are not represented and we want them to be.
- Whether or not to raise an event that is in the grays of the principles and rules that determine which ones are eligible.
- Updating these principles and rules.
- Moderate events of struggles in which we are not present.

Therefore we want the collective to grow to represent the diversity of the struggles of this city and that together we make these decisions.

For this we invite you to the next itinerant assembly of the collective that will be on `<< introduce day >>` in `<< introduce place >>`.

## Complete presentation of the project

Useful for a kafeta or a specific session to present the project.
