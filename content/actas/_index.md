+++
title = "Actas"
description = "Actas de las asambleas"
date = 2022-09-04T16:07:44+02:00
weight = 15
archetype = "section"
pre = "<b><i class='fa fa-file'></i> </b>"
tags = [
]
toc = true
+++

## Plantilla de orden del día de asamblea

* Reparto de roles de la asamblea (moderar y acta).
* Ronda de presentación.
* Anuncios del centro social.
* Balance general del mes
* Reparto de las tareas del centro social.
    * Asistencia a las asambleas de la ferro (aproximadamente cada 10 días)
    * Estar en el grupo de whassap de la Ferro
    * Cafetas / Limpieza
* Actualización de los grupos de trabajo.
    * Internet
    * Red local
    * Aprendizaje/Mentorias
    * Web
    * Sistemas de comunicación
    * Cuidados
    * Jornadas de privacidad
    * Encuadernación
    * Radio y radioafición
    * Openwrt y otros sistemas embebidos
    * "H4ck1ng"
    * Install party
* Día y hora de la siguiente asamblea.
