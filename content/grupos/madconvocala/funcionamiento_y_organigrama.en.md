+++
title = "Operation and organigram"
description = "How do we work? What are the parts of the project?"
date = 2023-05-05T16:07:44+02:00
weight = 15
chapter = false
tags = [
]
toc = true
+++

## Operation

- **Anonymous postings**. It is not necessary to have an account to upload a call. Calls published without an account will be reviewed by us. We try to publish them as soon as possible, but we are volunteers: if you are in a hurry, post them earlier next time.

- **Accounts by affinity groups**. If you share the [values charter]({{% ref "/grupos/madconvocala/carta_valores" %}} "Charter of principles"), you can write to us at <a href="mailto:mad.convocala<arroba>sindominio.net">`mad.convocala<arroba>sindominio.net`</a> and we will explain how to start publishing. Obviously, we reserve the right to say no if we think we are not compatible.

  Within our capabilities, we will invite groups that we already know or with whom we have relationships of trust and fellowship.

- **Tags**. The tag field is free, but try to use equivalent existing tags to facilitate future searches (e.g. #environment, #art, #LGTBIA+, #housing, #environmentalism, #concert, #kafeta). We try to have them follow this format:
  - Format of the event (talk, workshop, concert, demonstration, presentation, ...).
  - Thematic or struggle (anti-racism, feminism, housing, ...)
  - Neighborhood or district
  - Hashtags that have the poster or accompanying text
  - Attendance (not mixed, space for children)  - Collective/s that organizes

- **Occasional activities, periodical activities, or days**. Periodic activities such as workshops, assemblies, sports, etc, also have a place, but we ask for responsibility to keep updating them so that no one runs into a closed door.

  You can also publish as a single activity activities that last more than one day, such as conferences, parties, camping, events or gatherings.

## Organigram of mad.convoca.la

- **mad.convoca.la**. Collective of activists and militants that gives life to a public and federated activist and cultural agenda, it emerges from the hacklab of the Ferroviaria where you can find us on Wednesdays from 18:30-19.

- **Convoca.la**. Initiative promoted by sindominio.net to promote the use of Gancio as a digital tool for the self-organization of social movements. They manage the hosting of the tool, facilitating their own deployment for the territories that request it.

- **Sindominio.net**. "Sindominio is an autonomous and self-managed Internet server by a community of people who are committed to horizontal and secure forms of digital functioning. Sindominio was born in 1999, is organized as an assembly and today continues to be a space for mutual learning and technological empowerment. Sindominio provides activists with internet tools and resources such as email, mailing lists, instant messaging and web pages, among others."

- **Gancio**. Open source web application that aims to be a shared agenda for local communities. The project was born in Italy at [Hacklab Underscore](https://www.autistici.org/underscore/) which currently maintains the code. Relevant functionalities:

  - **Focused on content not on people**. Identity of the people who publish on Gancio does not appear anywhere, not even under a pseudonym or even to the admin users (except in the database). This is not an ego-boosting platform in that sense.

  - **Visits first**. We don't want people with an account to have more features at their disposal than a person who is visiting the site without registering. Furthermore, we do not want users to register except in case they want to post an event, for which there is also an anonymous and unregistered way to do so.

  - **Anonymous events**. Optionally, a person visiting the site can create an event without registering (an administrator must validate the event).

  - **We don't mind giving the news bombshell**. So we export our events in multiple ways, via RSS feeds, via individual or global ics, allowing you to create iframes to embed one or several events in your own web page, via AP, microdata and microformat.

  - Intuitive user interface (UI).

  - Events can be created to last several days

  - Recursive events can be created

  - Event filtering via a tag system

  - Export with RSS and ICS filters included

  - Possibility to embed events with web components and iframes

  - Event filtering by tags or locations.

  - Share, save and comment events from the fediverse!

  - Multiple configuration options (light and dark theme, open or close user registration, enable recursive events...

  - AGPL-3.0 Licence.