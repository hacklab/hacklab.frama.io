+++
title = "Página web estática vs CMS"
description = "Notas sobre la diferencia de una página web estática con un CMS."
date = 2022-09-23T16:07:44+02:00
weight = 7
chapter = false
pre = "<b> </b>"
tags = [
    "Web estática",
    "CMS"
]
toc = true
+++

## CMS

| **Pros**                                                                                                                                                                                           | **Contras**                                    | **Tecnologías**                  |
| :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :--------------------------------------------- | :------------------------------- |
| Mayor usabilidad  por usuarixs sin conocimientos tecnicos / Más habitual fuera de entornos técnicos (el esfuerzo de aprender a usarlo tiene mas probabilidades de ser util mas alla del hacklab) | Mayor complejidad, curva de aprendizaje media. | Wordpress (GPLv2 or later)  |
| Mayor escalabilidad                                                                                                                                                                                | Requiere de mantenimiento                      | Ghost                            |
| Mayor flexibilidad, infinidad de plugins gratuitos en el caso de WordPress                                                                                                                         | Menor seguridad                                |                                  |
| Documentación, recursos audiovisuales en varios idiomas... en resumen gran accesibilidad.                                                                                                          |                                                |                                  |

## Página Web Estática

| **Pros**                                                                          | **Contras**                                                                                                      | **Tecnologías** |
| :-------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------------------------------------- | :-------------- |
| Facilidad de despliegue gratuito (solamente hace falta servir archivos estáticos) | Usabilidad, se necesita saber usar Markdown y git y/o la interfaz gráfica de un sistema de control de versiones  | Hugo <br>       |
| No require mantenimiento.                                                         | No es tan personalizable                                                                                         | Jekyll          |
| Menor complejidad, menor curva de aprendizaje.                                    | Menos utilizado que otras alternativas                                                                           | Gitlab pages    |
| Mayor seguridad                                                                   |                                                                                                                  | Nikola          |
| Podemos hostear en framagit (empresa sin ánimo de lucro, leer abajo)              |                                                                                                                  | Pelican         |
|                                                                                   |                                                                                                                  | Mkdocs          |

## Alojamiento

### CMS

**Entidades/colectivos afines**

- Pros: 

    Tejer redes

- Contras:

    Entidades pequeñas: mayor volatibilidad, menos redundancia, soporte, etc.

### Páginas web estáticas

**Framagit**

- Pros: 

    Es un GitLab autoalojado por una comunidad sin ánimo de lucro (Framasoft). Al ser una asociación sin ánimo de lucro, no tienen ninguna intención de monetizar los datos que puedan recopilar al visitar la web alojada en sus servidores. "Framasoft es una red popular y educativa que utiliza software libre creada en 2001 por Alexis Kauffmann. Desde diciembre de 2003, cuenta con el apoyo de una asociación con el mismo nombre con sede en Lyon, Francia. Se divide en tres grandes ramas de actividades basadas en un modelo colaborativo: promoción, difusión y desarrollo de software libre, y enriquecimiento de la cultura del software libre y de los servicios en línea." https://es.wikipedia.org/wiki/Framasoft

- Contras: 

    Tardan 2 o 3 de días en validar una cuenta nueva.

**GitLab.com**

- Pros: 

    Es una plataforma Open Source https://gitlab.com/gitlab-org/gitlab y tiene un servicio de CI/CD potente y configurable (GitLab CI, se basa en imágenes de Docker)

- Contras: 

    Está mantenido y el alojamiento lo ofrece una empresa.

**GitHub**

- Pros: 

    Contribuyen al Open Source https://github.com/github y tienen un servicio de CI/CD configurable (GitHub Actions)

- Contras: 
    
    Es una plataforma privativa, el servicio está mantenido y el alojamiento lo ofrece una empresa. "Microsoft compró GitHub por la cantidad de 7500 millones de dólares."https://es.wikipedia.org/wiki/GitHub 

## Dominio

**Subdominio entidades/colectivos afines**

- Pros:

    Delegamos la responsabilidad de la gestión del domino.

- Contras:

    Perdida de autonomia y necesidad de colaboraciones adicionales.

**No-ip/servicio de nombres para ips dinámicas**

- Pros:

    Gratuito.

- Contras:

    Requiere una persona responsable de la cuenta con el proveedor.

    Requiere renovación mensual de forma manual por el adminstrador de la cuenta (por lo menos en No-Ip).

**Compra de dominio**

- Pros:

    Autonomia absoluta sobre la gestión del dominio.

    Podemos crear subdominios para las distintas organizaciones dentro de la ferro.

    Se puede elegir el nombre más apropiado.

- Contras: 

    Requiere de los datos personales de una entidad/persona

    Coste asociado anual

    Dependencia del proveedor del DNS (https://en.wikipedia.org/wiki/2021_Epik_data_breach)

**ENS: https://ens.domains/**

- Pros:

    Descentralizado.

    Compatible con IPFS como alternativa de hosting.

- Contras:

    Altamente experimental.

    Requiere capital (ether) para articularlo y ponerlo en funcionamiento.

    Las criptos tienen mala fama.

## Referencias

https://dev.to/ashenmaster/static-vs-dynamic-sites-61f