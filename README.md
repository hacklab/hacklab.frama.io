# Hacklab Uni Popular

Accesible en: [https://hacklab.frama.io/](https://hacklab.frama.io/)

[¿Quieres contribuir?](https://hacklab.frama.io/contribuir/) 

Si tienes algún error durante la instalación no dejes de visitar [editar con git](https://hacklab.frama.io/contribuir/editar_con_git/)

## Licencia

Código con GPLv3
Contenido con CC BY-NC-SA 4.0 