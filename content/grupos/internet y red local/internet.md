+++
title = "Internet"
description = "Notas sobre el proyecto de montar internet en la Ferro."
date = 2022-09-04T16:07:44+02:00
weight = 5
chapter = false
pre = "<b> </b>"
tags = [
]
toc = true
+++

## Decisiones



## Proveedores

### MANO

- Cooperativa (todavía no formada, por ahora gestionada mediante otra cooperativa)
- Visión crítica: Usuarias como productoras de información en lugar de consumidoras. Los beneficios de la producción se reinvierten en I+D.
- Ahora mismo es una operadora virtual y el servicio lo proporciona Más Movil. Tienen visión de crecer y dejar de depender de MM.
- Tiene permanencia de 1 año, se puede hacer un cambio de domicilio sin coste, y creen que sucesivos cambios son 50 pavos. Las multas por romper la permanencia serían de 120€ los primeros 6 meses y 60€ los segundos (dato también por confirmar).

Tarifas fibra + fijo:

100Mb 29.99€, 600Mb 39.99€, 1Gb 49.99€<

- Se puede aumentar la velocidad sin penalización.

## Cosas random

- Guifi.net: requiere montar una antena en la azotea, un router próximo con alimentación eléctrica, tener visibilidad con algún nodo existente y tender un cable de red entre el CSO y la azotea. Verificar que los nodos a los que tenemos acceso hay alguno que haga proxy hacia internet.

- Contratación de proveedor: no todos los proveedores dan servicio a CSOs. Tiene un coste recurrente, pero la conexión será estable.

Ejemplos de proovedor:
  https://somosconexion.coop/tarifas-internet/

- Router 4G o 5G, tiene coste mensual de la tarjeta, da menos conectividad que la fibra pero tenemos menos problemas con quién la contrata. Algunos ejemplos:

https://orange.altas-internet.com/ORANGE_4GencasaAltasnuevas_Internet_2_0?encid=0hiSnYhJ0nDiananbqEW2A%253d%253d&c-sig=1652954771-18655680

https://landing.altas-internet.com/Eurona_4Gencasa200GB_Internet_2_0?encid=0hiSnYhJ0nDiananbqEW2A%253d%253d&c-sig=1652954771-18655680

https://www.movistar.es/particulares/internet/adsl-fibra-optica/movistar-internet-radio/

https://wifiaway.es/

https://www.digimobil.es/ilimitodo

https://www.vodafone.es/c/particulares/es/productos-y-servicios/movil/contrato/tarifas-contrato/tarifa-3/

- ¿Nos interesa cifrar todo el tráfico de salida, quizá pasándolo por Tor / VPN / https://yggdrasil-network.github.io/?