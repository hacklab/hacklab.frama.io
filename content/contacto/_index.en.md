+++
title = "Contact"
description = "Do you want to meet us?"
date = 2022-09-04T16:07:44+02:00
weight = 25
archetype = "section"
pre = "<b><i class='fas fa-envelope'></i> </b>"
tags = [
]
toc = true
+++

Every Wednesday from 6pm to 9pm at the CSOA La Ferroviaria (Plaza Luca de Tena, 7).

{{% button href="https://t.me/+EmWc6-CNLPJlMTc0" style="blue" icon="fab fa-telegram" %}}Join us in Telegram!{{% /button %}}

Or write any questions to the following e-mail address: <a href="mailto:hacklab-ferro@riseup.net">`hacklab-ferro<arroba>riseup.net`</a>.

Come and meet us any day there is a hacklab event :)

{{< hacklab-events >}}

{{< openstreetmap mapName="csoa-la-ferroviaria_985641" >}}