
+++
title = "Materiales de presentación"
description = "Cómo transmitir el proyecto a los colectivos"
date = 2023-06-01T16:07:44+02:00
weight = 30
chapter = false
tags = [
]
toc = true
+++

Dependiendo del contexto en el que estemos para presentar el proyecto de mad.convoca.la a los colectivos hay diferentes formatos que podemos usar:

- [Fanzine](#fanzine)
- [Mensaje de difusión general](#mensaje-de-difusión-general)
- [Invitación a subir eventos de cada colectivo propio](#invitación-a-subir-eventos-de-cada-colectivo-propio)
- [Invitación a usar la herramienta y pasarse por la asamblea](#invitación-a-usar-la-herramienta-y-pasarse-por-la-asamblea)
- [Mensaje de contacto con colectivos cuando subimos un evento suyo](#mensaje-de-contacto-con-colectivos-cuando-subimos-un-evento-suyo)
- [Presentación en un punto de asamblea](#presentación-en-un-punto-de-asamblea)
  - [Presentación del proyecto](#presentación-del-proyecto)
  - [Presentación de la página](#presentación-de-la-página)
  - [Presentación del colectivo](#presentación-del-colectivo)
- [Protocolo de Difusión de Carteles en un Centro Social](#protocolo-de-difusión-de-carteles-en-un-centro-social)
    - [\[Parte de Bienvenida\]](#parte-de-bienvenida)
    - [\[Parte de Comunicación\]](#parte-de-comunicación)
- [Lista de correo](#lista-de-correo)
- [Presentación completa del proyecto](#presentación-completa-del-proyecto)

## Fanzine

¡Os presentamos nuestro fanzine sobre mad.convoca.la!
En esta publiacion hemos sintetizado el qué y el por qué de nuestra instancia de Gancio. La idea es darla a conocer a diferentes colectivos para que sepan de su existencia y sus capacidades. Sentíos libres de difundirla :)

{{< embed-pdf url="/js/madconvocala/Fanzine_madconvocala.pdf" >}}

## Mensaje de difusión general

Se puede reenviar [este mensaje](https://t.me/madconvocala/113) del canal de telegram, enviar el enlace de la [introducción del proyecto](https://hacklab.frama.io/grupos/madconvocala/mad.convoca.la/).

## Invitación a subir eventos de cada colectivo propio


## Invitación a usar la herramienta y pasarse por la asamblea

Si os interesa empezar a hacer uso de la herramienta os invitamos a pasaros por una de nuestras asambleas para conocernos en persona y poder facilitaros cualquier información que necesitéis. O a contactar con nosotras a través de correo electrónico a `mad.convocala<arroba>sindominio.net`.

## Mensaje de contacto con colectivos cuando subimos un evento suyo

Para encontrar la lista de colectivos con los que hemos contactado visita [este pad](https://pad.riseup.net/p/convocala-contacto-colectivos-keep), ahí también puedes encontrar la versión más actualizada del mensaje a enviar.

```
Hola @NombreColectivo, 

Acabamos de subir vuestro evento a https://mad.convoca.la. Esperamos que no os importe 😜, si no es así, comentanoslo y lo borramos :). Podéis verlo aquí: 
    
<<Link al evento>>

Decidnos si véis alguna mejora en los datos del evento.

Somos el colectivo mad.convoca.la (http://mad.convoca.la/) y estamos impulsando una agenda política y contracultural en Madrid. Velamos por la okupación digital de los espacios de comunicación y autoorganización. De forma que intentamos dar apoyo a la difusión comunitaria a los eventos de los movimientos sociales.

Si queréis contribuir, podéis empezar a subir vuestros eventos o difundir la web. El boca a boca siempre es la mejor estrategia. También os invitamos a nuestra asamblea online o itinerante y abierta a colectivos.

Para saber más sobre el proyecto, aquí tenéis más información: https://hacklab.frama.io/grupos/madconvocala y https://mad.convoca.la/about.

No dudéis en comentarnos cualquier duda o mejora que os surja,
Un abrazo
```

Opcional:

```
También hemos visto que tenéis un calendario en la web con Wordpress. Existe una extensión para Wordpress que os permitiría subir los eventos automáticamente a mad.convoca.la. Si os interesa podemos ayudaros a instarlo.
```

## Presentación en un punto de asamblea 

Útil para cuando presentamos el proyecto en la asamblea del propio colectivo a contactar, o como introducción si hay gente nueva en la asamblea de mad.convoca.la. La idea es que no lleve más de 10 minutos

### Presentación del proyecto

Somos un colectivo que está creando una **agenda política y cultural autónoma**, que quiere contribuir a **difundir y coordinar acciones** públicas de colectivos sociales y políticos en **Madrid**. Para sacar adelante el proyecto usamos dos herramientas [una página web](#presentación-de-la-pagina) y [el colectivo](#presentación-del-colectivo).

### Presentación de la página

En [la página web](https://mad.convoca.la) se pueden ver y subir los eventos de forma amigable e intuitiva. Al estar hecha específicamente para este fin tiene funcionalidades muy interesantes como:

- **Automatización de difusión**. La página tiene mecanismos para propagar la información de manera automática cuando se suba o edite un evento. Es capaz de publicar eventos en:

  - [Canales de telegram](https://t.me/madconvocala).
  - [La página del colectivo](https://hacklab.frama.io/agenda/).
  - [El calendario de las activistas](https://mad.convoca.la/feed/ics?show_recurrent=true).
  - [En redes sociales libres](https://mastodon.social/@madrid@mad.convoca.la).
  - [En lectores de RSS](https://mad.convoca.la/export).

- **Facilidad de creación de eventos**. A través de:

  - [La propia web](https://mad.convoca.la/add)
  - El plugin de Wordpress 
  - El bot de telegram. 

  Tienes las siguientes ayudas a la hora de subir un evento:

  - Autocompletado de tags y lugares.
  - Aviso sobre eventos ya convocados para evitar pisarnos.
  - Selectores gráficos de fecha y hora.
  - Selección de fragmento de imagen a mostrar.

- **Facilidad de visualización de eventos**. A través de:

  - Página principal con resumen de todos los eventos.
  - Filtrado de por barrios, lugares, tags o fechas.
  - Página específica de cada evento 
  - Ver en un mapa dónde va a ser el evento.
  - Creando rutas andando, en bici o coche para saber cómo llegar.

- **Creación anónima de eventos**. Por si no quieres crearte cuenta o que se te asocie con el evento.

- **Recuperar la soberanía comunicativa**. La página está alojada en servidores radicales ([Sin Dominio](https://sindominio.net/)), utilizamos una herramienta de código libre y colaborativa ([Gancio](https://gancio.org/)) y un colectivo asambleario ([Convócala Madrid](https://hacklab.frama.io/grupos/madconvocala/mad.convoca.la/)) decide qué se publica y qué no. De manera que no dependemos de las redes y filtros capitalistas para difundir nuestros eventos.

### Presentación del colectivo

Desde el colectivo promotor hemos establecido una serie de [principios](https://hacklab.frama.io/grupos/madconvocala/carta_valores/#principios) que los eventos han de cumplir para ser admitidos. No obstante en el día a día de la gestión de la agenda surgen debates y decisiones políticas que no sentimos que tengamos la potestad de tomar solas. Por ejemplo:

- Qué colectivos no están representados y queremos que estén.
- Si subir o no un evento que está en los grises de los principios y normas que determinan cuales son aptos.
- Actualización de dichos principios y normas.
- Moderar eventos de luchas en las que no estamos presentes.

Por ello queremos que el colectivo crezca hasta representar la diversidad de las luchas de esta ciudad y que entre todas tomemos estas decisiones.

Para ello os invitamos a la siguiente asamblea itinerante del colectivo que va a ser el `<< introducir día >>` en `<< introducir lugar >>`. 


## Protocolo de Difusión de Carteles en un Centro Social

Esta siguiente sección es una propuesta para un protocolo que facilite la difusión de eventos y actividades de un Centro Social. Para ello, hay dos comisiones que intervienen: Bienvenida (gestión de actividades y calendarización) y Comunicación (gestión de redes) de un centro social.

#### [Parte de Bienvenida]

1. Una vez que bienvenida haya dado el visto bueno a una actividad, propondrá al colectivo que lo suba a mad.convoca.la. Es importante resaltar que ayuda a coordinarse para difundirlo por todas las redes del centro social. Sirviendo como fuente de verdad y dando autonomía a los colectivos para realizar modificaciones. Existe esta guía de [cómo subir un evento](https://hacklab.frama.io/grupos/madconvocala/guias/publicar_evento/). Si es una actividad puntual se recomienda subirla anónimamente. En cambio, si es una actividad recurrente conviene regístrarse para mantenerla actualizada, ya sea por modificar los datos del evento o en caso de que algún día se cancele.

#### [Parte de Comunicación]

2. Una vez subido, se publicará automáticamente en el canal de telegram. Por ejemplo: https://t.me/convocalaeko. Para difundirlo por las redes sociales de la eko y notificar que ya se ha subido, se podría reaccionar con un emoticono concreto cuando se publique o se programe en cada plataforma. Otra opción es crear un grupo de discusión asociado al canal e ir comentando por ahí los detalles de difusión. Por ejemplo, los emoticonos asociados a cada plataforma podrían ser:
- Mastodon 🦄
- Twitter 🐳
- Instagram 👻
- Telegram 🍓

## Lista de correo

Tenemos una lista de correo donde mandaremos recordatorios de las asambleas, resúmenes de avances del proyecto. Además, son bienvenidos mensajes que sean de interés para el grupo en general. Desde la asamblea hemos decidido que todos los colectivos con cuenta en mad.convoca.la deben estar suscritos a esta lista de correo para que podamos  comunicarnos (nos comprometemos a mandar solo lo justo y necesario). Por este motivo, si no os apuntáis entendemos que no queréis continuar en mad.convoca.la y eliminaremos vuestra cuenta. Aquí está la lista de correo:

[https://listas.sindominio.net/mailman/listinfo/mad.convoca.la](https://listas.sindominio.net/mailman/listinfo/mad.convoca.la)

Al introducir el email, os llegará un mensaje de confirmación para la subscripción a la lista de
distribución mad.convoca.la. Pincharemos en el enlace obtenido y recibiremos el siguiente mensaje de bienvenida:

```
¡Hola desde mad.convoca.la!

Os damos la bienvenida a nuestra lista de correo 😄, desde aquí podréis estar al tanto de las novedades y eventos internos que organicemos. Estad atentas porque este año queremos darle un buen empujón al proyecto y llegar a más colectivos que nunca 🚀

Que corra la voz, los movimientos sociales ya tenemos una plataforma de eventos independiente y hecha pensando en nosotras. ✊🏾✊🏻✊🏽

⚠️⚠️⚠️ Desde la asamblea hemos decidido que todos los colectivos con cuenta en mad.convoca.la deben estar suscritos a esta lista de correo para que podamos  comunicarnos (nos comprometemos a mandar solo lo justo y necesario). Por este motivo, si no os apuntáis entendemos que no queréis continuar en mad.convoca.la y eliminaremos vuestra cuenta. Seguid los pasos que encontraréis más abajo ⬇️

Para más información visita: https://hacklab.frama.io/grupos/madconvocala/

Para cualquier duda, pásate por la [siguiente asamblea online](https://mad.convoca.la/tag/Mad%20Conv%C3%B3cala) o escribe al correo mad.convocala@sindominio.net 

¡Anímaos a participar!

Salud y difusión 📣
```

## Presentación completa del proyecto

Útil para una kafeta o una sesión específica para dar a conocer el proyecto.