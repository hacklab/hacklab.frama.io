+++
title = "Actas"
description = "Actas del grupo de cuidados"
date = 2022-09-04T16:07:44+02:00
weight = 10
chapter = false
pre = "<b> </b>"
tags = [
]
toc = true
+++

# Miniespacio de cuidados 2022-07-20

Propuesta: Generar un espacio cada encuentro de entre 15 y 20 minutos de crítica
y autocrítica para plantear nuestros sentires y poder ir trabajando de a poco
nuestra perspectiva en torno a la crítica y al como gestionarla (a nivel
personal y colectivo). La idea es que se haga cada sesión para que sea una
construcción continua y constante pero sin que se haga pesado (en sí y el que
alguien tenga que preparar dinámicas y etc.), pero que en caso de que lo
sintamos necesario, hacer una reunión larga para tratar algo en particular.

Estaría pensado a las 20, e intentaríamos garantizarla entre todas, es decir,
que quien se dé cuenta de que es el horario recuerde que es el momento de
cuidados.

El plantear qué situaciones consideramos adecuadas y cuáles inadecuadas y el
cómo tratar situaciones específicas nos parece mejor dejarlo para después de
verano, pero teniendo en cuenta que es algo necesario y no hay que olvidarnos de
ello.

# Miniespacio de cuidados 2022-07-13

Propuestas:

- Necesitamos una actitud activa para que el hackLab sea realmente un espacio
    seguro.
- Hay actitudes que pueden generar incomodidades y romper ese "espacio seguro".
- Se pueden definir en un manifiesto.
- También escribir pequeñas acciones/respuestas cuando se den circunstancias
    como mini protocolos.
- Dedicar unos minutos a un miniespacio de cuidados cada miércoles.
- Pedir consejo o apoyo a gente de la ferro, asambleas feministas, otros
    colectivos.
    - Podríamos tirar de grupos de hombres y empezar con trabajo propio,
        requiere un esfuerzo extra, e incluso se podría montar un grupo de
        "masculinidades" fuera del hacklab.
- Que las personas del hacklab puedan escribir de forma anónima temas a tratar.

- ¿Cómo se genera un espacio de diálogo para tratar estos temas? Sin que suponga hablar a espaldas de otras personas que pueden ser amigas, conocidas...
- Aprender a cuidar -> poner foco en empatizar.
- Tratar de que las personas que llegan al espacio (nueva o no) se sientan
    cómodas.
- ¿Cómo se puede hacer seguimiento cuando hay gente que se va del colectivo? ¿Puede ser que se haya sentido incómoda?
- ¿Cómo hacer para intervenir en positivo - no ir a "llamar la atención", si no preguntar "¿cómo podemos hacer?, ¿por qué te estás comportando así..?"
- Buscar equilibrio entre cuidados / intervención - (no convertir el hacklab en un espacio de terapia)

Qué rasgos definen un espacio como seguro:

- Libre de violencias de todo tipo
- No ocurren comentarios (micro)machistas, (micro)xenófobos, ni micro(homófobos)...
- Siempre abierto a la escucha
- ... pendiente definir más

Qué podemos hacer ante determinadas conductas puntuales:

- Ejemplos/consejos.
- Recordar amablemente cuando hay un turno de palabra pedido.
- Intervenir si interrumpimos a la persona que está hablando utilizando un tono de voz más alto ?
- Dar bienvenida/acoger a una persona nueva que llega al espacio

Actitudes
- Comentarios/actitudes/comportamientos machistas - ¿qué hacemos? - tratar de intervenir de forma pedagógica... ¿cómo?
- Alguien suelta un comentario sexualmente agresivo -> no seguir la gracia - evitar retroalimientación
- En cualquier caso, que la intervención sea
    - Positiva/constructiva/pedagógica para la persona que la recibe
    - Sirva para evitar otras actuaciones similares en el futuro
