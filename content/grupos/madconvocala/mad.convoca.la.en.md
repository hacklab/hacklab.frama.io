+++
title = "🔴 What is mad.convoca.la? 🔥"
description = "Want to know more or get to know us?"
date = 2023-05-05T16:07:44+02:00
weight = 10
chapter = false
tags = [
]
toc = true
+++

It is a collective (of collectives) that disseminates practices of **communicative sovereignty**. We are currently promoting an **autonomous political and cultural agenda**, which wants to contribute to **disseminate and coordinate public actions** of social and political collectives in **Madrid**.

## 🌐 Pillars of mad.convoca.la 📣
- Digital squatting of **communication and self-organization** spaces.
- Support for **community broadcasting** of social movements.
- For communication between collectives, **for more interlinking and less stepping on each other's toes**.
- To **facilitate the personal planning** of activists.

## 💻 Technology 🛠
- **Self-managed**: which returns content management to the community and collectives that generate these proposals, both cultural and social.
- **Independent (but not exclusive)**: an *alternative response* to platforms such as Facebook, Instagram, Telegram or Twitter. Which do mass surveillance, political censorship, social manipulation (advertising), and content control that is not very transparent and has been proven to favor economic powers.
- **Privacy-friendly**: minimization of collected data, collective identities, anonymous publications.
- **Autonomous**: we use an open source and collaborative tool ([Gancio](https://gancio.org/)), hosted on radical servers ([Sin Dominio](https://sindominio.net/)).

## 🤔 Where to start? ▶️
- Come to the [**autonomous online or itinerant assembly**](<https://mad.convoca.la/tag/Mad Convócala>) in different spaces.
- Visit the **website [https://mad.convoca.la](https://mad.convoca.la)**.
- In the **fediverse**, follow the account `@madrid@mad.convoca.la`.
- Follow the **telegram channel** [t.me/madconvocala](https://t.me/madconvocala).
- Import the **remote calendar** ICS/ICAL: [https://mad.convoca.la/feed/ics?show_recurrent=true](https://mad.convoca.la/feed/ics?show_recurrent=true)
- Add this link to your **RSS feed reader**: [https://mad.convoca.la/feed/rss?show_recurrent=true](https://mad.convoca.la/feed/rss?show_recurrent=true)

## 📆 When? ⏰
Every day of the year 🔥

To learn more about the project, here is all the information on this website and at [https://mad.convoca.la/about](https://mad.convoca.la/about).
